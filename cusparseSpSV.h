#include "common.h"
#include <cuda_runtime.h>
#include "cusparse.h"

#define VALUE_TYPE double

#define CHECK_CUDA(func)                                                       \
{                                                                              \
    cudaError_t status = (func);                                               \
    if (status != cudaSuccess) {                                               \
        printf("CUDA API failed at line %d with error: %s (%d)\n",             \
               __LINE__, cudaGetErrorString(status), status);                  \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}

#define CHECK_CUSPARSE(func)                                                   \
{                                                                              \
    cusparseStatus_t status = (func);                                          \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                   \
        printf("CUSPARSE API failed at line %d with error: %s (%d)\n",         \
               __LINE__, cusparseGetErrorString(status), status);              \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}


int sptrsv_cusparse(const int           *csrRowPtrL_tmp,
                  const int           *csrColIdxL_tmp,
                  const MAT_VAL_TYPE    *csrValL_tmp,
                  const int            m,
                  const int            n,
                  const int            nnzL,
                        MAT_VAL_TYPE         *x, 
                  const MAT_VAL_TYPE         *b, 
                  const MAT_VAL_TYPE         *x_ref,
                  char                 *filename)
                        // double        *preprocessing_csrsv,
                        // double        *runtime_csrsv,
                        // double        *gflops_csrsv,
                        // double        *preprocessing_csrsv2,
                        // double        *runtime_csrsv2,
                        // double        *gflops_csrsv2
                        
{
    timeval t1, t2;
    // transfer host mem to device mem
    int *d_csrRowPtr;
    int *d_csrColInd;
    MAT_VAL_TYPE *d_csrVal;
    MAT_VAL_TYPE *d_b;
    MAT_VAL_TYPE *d_x;

    // Matrix L
    cudaMalloc((void **)&d_csrRowPtr, (m+1) * sizeof(int));
    cudaMalloc((void **)&d_csrColInd, nnzL  * sizeof(int));
    cudaMalloc((void **)&d_csrVal,    nnzL  * sizeof(MAT_VAL_TYPE));

    cudaMemcpy(d_csrRowPtr, csrRowPtrL_tmp, (m+1) * sizeof(int),   cudaMemcpyHostToDevice);
    cudaMemcpy(d_csrColInd, csrColIdxL_tmp, nnzL  * sizeof(int),   cudaMemcpyHostToDevice);
    cudaMemcpy(d_csrVal,    csrValL_tmp,    nnzL  * sizeof(MAT_VAL_TYPE),   cudaMemcpyHostToDevice);

    // Vector b
    cudaMalloc((void **)&d_b, m * sizeof(MAT_VAL_TYPE));
    cudaMemcpy(d_b, b, m * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);

    // Vector x
    cudaMalloc((void **)&d_x, n  * sizeof(MAT_VAL_TYPE));
    cudaMemset(d_x, 0, n * sizeof(MAT_VAL_TYPE));

    gettimeofday(&t1, NULL);
    MAT_VAL_TYPE     alpha           = 1.0f;    // CUSPARSE APIs
    cusparseHandle_t     handle = NULL;
    cusparseSpMatDescr_t matA;
    cusparseDnVecDescr_t vecX, vecY;
    void*                dBuffer    = NULL;
    size_t               bufferSize = 0;
    cusparseSpSVDescr_t  spsvDescr;
    CHECK_CUSPARSE( cusparseCreate(&handle) )
    // Create sparse matrix A in CSR format

    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseCreateCsr(&matA, m, n, nnzL,
                                        d_csrRowPtr, d_csrColInd, d_csrVal,
                                        CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                        CUSPARSE_INDEX_BASE_ZERO, CUDA_R_64F) )
    else
        CHECK_CUSPARSE( cusparseCreateCsr(&matA, m, n, nnzL,
                                        d_csrRowPtr, d_csrColInd, d_csrVal,
                                        CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                        CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F) )
    // Create dense vector X
    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseCreateDnVec(&vecX, n, d_b, CUDA_R_64F) )
    else
        CHECK_CUSPARSE( cusparseCreateDnVec(&vecX, n, d_b, CUDA_R_32F) )
    // Create dense vector y
    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseCreateDnVec(&vecY, m, d_x, CUDA_R_64F) )
    else
        CHECK_CUSPARSE( cusparseCreateDnVec(&vecY, m, d_x, CUDA_R_32F) )
    // Create opaque data structure, that holds analysis data between calls.
    CHECK_CUSPARSE( cusparseSpSV_createDescr(&spsvDescr) )
    // Specify Lower|Upper fill mode.
    cusparseFillMode_t fillmode = CUSPARSE_FILL_MODE_LOWER;
    CHECK_CUSPARSE( cusparseSpMatSetAttribute(matA, CUSPARSE_SPMAT_FILL_MODE,
                                              &fillmode, sizeof(fillmode)) )
    // Specify Unit|Non-Unit diagonal type.
    cusparseDiagType_t diagtype = CUSPARSE_DIAG_TYPE_NON_UNIT;
    CHECK_CUSPARSE( cusparseSpMatSetAttribute(matA, CUSPARSE_SPMAT_DIAG_TYPE,
                                              &diagtype, sizeof(diagtype)) )
    // allocate an external buffer for analysis
    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseSpSV_bufferSize(
                                    handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                    &alpha, matA, vecX, vecY, CUDA_R_64F,
                                    CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr,
                                    &bufferSize) )
    else
        CHECK_CUSPARSE( cusparseSpSV_bufferSize(
                                    handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                    &alpha, matA, vecX, vecY, CUDA_R_32F,
                                    CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr,
                                    &bufferSize) )
    CHECK_CUDA( cudaMalloc(&dBuffer, bufferSize) )

    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseSpSV_analysis(
                                    handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                    &alpha, matA, vecX, vecY, CUDA_R_64F,
                                    CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr, dBuffer) )
    else
        CHECK_CUSPARSE( cusparseSpSV_analysis(
                                    handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                    &alpha, matA, vecX, vecY, CUDA_R_32F,
                                    CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr, dBuffer) )
    
    for (int i = 0; i < WARMUP_NUM; i++)
    {
        // execute SpSV
    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseSpSV_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                        &alpha, matA, vecX, vecY, CUDA_R_64F,
                                        CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr) )
    else
        CHECK_CUSPARSE( cusparseSpSV_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                        &alpha, matA, vecX, vecY, CUDA_R_32F,
                                        CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr) )
        cudaDeviceSynchronize();
    }
    gettimeofday(&t2, NULL);
    double pretime = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;


    // for (int i = 0; i < WARMUP_NUM; i++)
    // {
    //     // execute SpSV
    //     CHECK_CUSPARSE( cusparseSpSV_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
    //                                     &alpha, matA, vecX, vecY, CUDA_R_64F,
    //                                     CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr) )
    //     cudaDeviceSynchronize();
    // }

    gettimeofday(&t1, NULL);
    for (int i = 0; i < BENCH_REPEAT; i++)
    {
        // execute SpSV
    if (sizeof(MAT_VAL_TYPE)==8)
        CHECK_CUSPARSE( cusparseSpSV_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                        &alpha, matA, vecX, vecY, CUDA_R_64F,
                                        CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr) )
    else
    {
        CHECK_CUSPARSE( cusparseSpSV_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                        &alpha, matA, vecX, vecY, CUDA_R_32F,
                                        CUSPARSE_SPSV_ALG_DEFAULT, spsvDescr) )
        // printf("float\n");
    }
        cudaDeviceSynchronize();
    }
    gettimeofday(&t2, NULL);
    double runtime = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    runtime /= BENCH_REPEAT;
    double gflops = (2.0 * nnzL * 1.0e-6) / runtime;
    
    // int level_fake = 0;
    // long long  malloc_size = (m+1) * sizeof(int) + nnzL  * sizeof(int) + nnzL  * sizeof(double);
    // FILE *fout = fopen("res/4090/cusparse.csv", "a");
    // fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f",
    //         m, n, nnzL, level_fake, malloc_size, pretime, runtime, gflops);
    // fclose(fout);

    // destroy matrix/vector descriptors
    CHECK_CUSPARSE( cusparseDestroySpMat(matA) )
    CHECK_CUSPARSE( cusparseDestroyDnVec(vecX) )
    CHECK_CUSPARSE( cusparseDestroyDnVec(vecY) )
    CHECK_CUSPARSE( cusparseSpSV_destroyDescr(spsvDescr));
    CHECK_CUSPARSE( cusparseDestroy(handle) )

    // device result check
    CHECK_CUDA( cudaMemcpy(x, d_x, m * sizeof(MAT_VAL_TYPE),
                           cudaMemcpyDeviceToHost) )

    int correct = 1;
    for (int i = 0; i < m; i++) {
        if (x[i] != x_ref[i]) { // direct floating point comparison is not
            correct = 0;             // reliable
            break;
        }
    }
    if (correct)
        printf("cuSPARSE test PASSED\n");
    else
        printf("cuSPARSE test FAILED: wrong result\n");

    int level_fake = 0;
    long long  malloc_size = (m+1) * sizeof(int) + nnzL  * sizeof(int) + nnzL  * sizeof(MAT_VAL_TYPE);
    FILE *fout = fopen("res/4090/cusparse.csv", "a");
    fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f, %i",
            m, n, nnzL, level_fake, malloc_size, pretime, runtime, gflops, correct);
    fclose(fout);

    // step 6: free resources
    cudaFree(d_csrRowPtr);
    cudaFree(d_csrColInd);
    cudaFree(d_csrVal);
    cudaFree(d_b);
    cudaFree(d_x);

    // cudaFree(pBuffer);
    //cusparseDestroySolveAnalysisInfo(csrsv_info);
    // cusparseDestroyCsrsv2Info(info);
    // cusparseDestroyMatDescr(descr);
    // cusparseDestroy(handle);
    // device memory deallocation
    CHECK_CUDA( cudaFree(dBuffer) )
    // CHECK_CUDA( cudaFree(dA_csrOffsets) )
    // CHECK_CUDA( cudaFree(dA_columns) )
    // CHECK_CUDA( cudaFree(dA_values) )
    // CHECK_CUDA( cudaFree(dX) )
    // CHECK_CUDA( cudaFree(dY) )

    return 0;
}





