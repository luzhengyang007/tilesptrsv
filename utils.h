#ifndef _UTILS_
#define _UTILS_

#include "common.h"
#include "findlevel.h"
#include "tranpose.h"
#define SUBSTITUTION_FORWARD  0
#define SUBSTITUTION_BACKWARD 1
#define MAT_VAL_TYPE float


void binary_search_right_boundary_item_kernel(const MAT_PTR_TYPE *row_pointer,
                                              const MAT_PTR_TYPE key_input,
                                              const int size,
                                              int *colpos,
                                              MAT_PTR_TYPE *nnzpos)
{
    int start = 0;
    int stop = size - 1;
    MAT_PTR_TYPE median;
    MAT_PTR_TYPE key_median;

    while (stop >= start)
    {
        median = (stop + start) / 2;

        key_median = row_pointer[median];

        if (key_input >= key_median)
            start = median + 1;
        else
            stop = median - 1;
    }

    *colpos = start - 1;
    *nnzpos = key_input - row_pointer[*colpos];
}

// in-place exclusive scan
// void exclusive_scan(MAT_PTR_TYPE *input, int length)
// {
//     if (length == 0 || length == 1)
//         return;

//     MAT_PTR_TYPE old_val, new_val;

//     old_val = input[0];
//     input[0] = 0;
//     for (int i = 1; i < length; i++)
//     {
//         new_val = input[i];
//         input[i] = old_val + input[i - 1];
//         old_val = new_val;
//     }
// }

// void exclusive_scan_char(unsigned char *input, int length)
// {
//     if (length == 0 || length == 1)
//         return;

//     unsigned char old_val, new_val;

//     old_val = input[0];
//     input[0] = 0;
//     for (int i = 1; i < length; i++)
//     {
//         new_val = input[i];
//         input[i] = old_val + input[i - 1];
//         old_val = new_val;
//     }
// }

/*
// in-place exclusive scan
void exclusive_scan_int(int *input, int length)
{
    if(length == 0 || length == 1)
        return;

    int old_val, new_val;

    old_val = input[0];
    input[0] = 0;
    for (int i = 1; i < length; i++)
    {
        new_val = input[i];
        input[i] = old_val + input[i-1];
        old_val = new_val;
    }
}
*/

void swap_key(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap_val(MAT_VAL_TYPE *a, MAT_VAL_TYPE *b)
{
    MAT_VAL_TYPE tmp = *a;
    *a = *b;
    *b = tmp;
}

// quick sort key-value pair (child function)
int partition_key_val_pair(int *key, MAT_VAL_TYPE *val, int length, int pivot_index)
{
    int i = 0;
    int small_length = pivot_index;

    int pivot = key[pivot_index];
    swap_key(&key[pivot_index], &key[pivot_index + (length - 1)]);
    swap_val(&val[pivot_index], &val[pivot_index + (length - 1)]);

    for (; i < length; i++)
    {
        if (key[pivot_index + i] < pivot)
        {
            swap_key(&key[pivot_index + i], &key[small_length]);
            swap_val(&val[pivot_index + i], &val[small_length]);
            small_length++;
        }
    }

    swap_key(&key[pivot_index + length - 1], &key[small_length]);
    swap_val(&val[pivot_index + length - 1], &val[small_length]);

    return small_length;
}

// quick sort key-value pair (main function)
void quick_sort_key_val_pair(int *key, MAT_VAL_TYPE *val, int length)
{
    if (length == 0 || length == 1)
        return;

    int small_length = partition_key_val_pair(key, val, length, 0);
    quick_sort_key_val_pair(key, val, small_length);
    quick_sort_key_val_pair(&key[small_length + 1], &val[small_length + 1], length - small_length - 1);
}

// quick sort key (child function)
int partition_key(int *key, int length, int pivot_index)
{
    int i = 0;
    int small_length = pivot_index;

    int pivot = key[pivot_index];
    swap_key(&key[pivot_index], &key[pivot_index + (length - 1)]);

    for (; i < length; i++)
    {
        if (key[pivot_index + i] < pivot)
        {
            swap_key(&key[pivot_index + i], &key[small_length]);
            small_length++;
        }
    }

    swap_key(&key[pivot_index + length - 1], &key[small_length]);

    return small_length;
}

// quick sort key (main function)
void quick_sort_key(int *key, int length)
{
    if (length == 0 || length == 1)
        return;

    int small_length = partition_key(key, length, 0);
    quick_sort_key(key, small_length);
    quick_sort_key(&key[small_length + 1], length - small_length - 1);
}

void matrix_transposition(const int m,
                          const int n,
                          const MAT_PTR_TYPE nnz,
                          const MAT_PTR_TYPE *csrRowPtr,
                          const int *csrColIdx,
                          const MAT_VAL_TYPE *csrVal,
                          int *cscRowIdx,
                          MAT_PTR_TYPE *cscColPtr,
                          MAT_VAL_TYPE *cscVal)
{
    // histogram in column pointer
    memset(cscColPtr, 0, sizeof(MAT_PTR_TYPE) * (n + 1));
    for (MAT_PTR_TYPE i = 0; i < nnz; i++)
    {
        cscColPtr[csrColIdx[i]]++;
    }

    // prefix-sum scan to get the column pointer
    exclusive_scan(cscColPtr, n + 1);

    MAT_PTR_TYPE *cscColIncr = (MAT_PTR_TYPE *)malloc(sizeof(MAT_PTR_TYPE) * (n + 1));
    memcpy(cscColIncr, cscColPtr, sizeof(MAT_PTR_TYPE) * (n + 1));

    // insert nnz to csc
    for (int row = 0; row < m; row++)
    {
        for (MAT_PTR_TYPE j = csrRowPtr[row]; j < csrRowPtr[row + 1]; j++)
        {
            int col = csrColIdx[j];

            cscRowIdx[cscColIncr[col]] = row;
            cscVal[cscColIncr[col]] = csrVal[j];
            cscColIncr[col]++;
        }
    }

    free(cscColIncr);
}

void matrix_transposition_char(const int m,
                          const int n,
                          const MAT_PTR_TYPE nnz,
                          const unsigned char *csrRowPtr,
                          const unsigned char *csrColIdx,
                          const MAT_VAL_TYPE *csrVal,
                          unsigned char *cscRowIdx,
                          unsigned char *cscColPtr,
                          MAT_VAL_TYPE *cscVal)
{
    // histogram in column pointer
    memset(cscColPtr, 0, sizeof(unsigned char) * (n + 1));
    for (MAT_PTR_TYPE i = 0; i < nnz; i++)
    {
        cscColPtr[csrColIdx[i]]++;
    }

    // prefix-sum scan to get the column pointer
    exclusive_scan_char(cscColPtr, n + 1);

    unsigned char *cscColIncr = (unsigned char *)malloc(sizeof(unsigned char) * (n + 1));
    memcpy(cscColIncr, cscColPtr, sizeof(unsigned char) * (n + 1));

    // insert nnz to csc
    for (int row = 0; row < m; row++)
    {
        for (unsigned char j = csrRowPtr[row]; j < csrRowPtr[row + 1]; j++)
        {
            unsigned char col = csrColIdx[j];

            cscRowIdx[cscColIncr[col]] = row;
            cscVal[cscColIncr[col]] = csrVal[j];
            cscColIncr[col]++;
        }
    }

    free(cscColIncr);
}



void swap_int(int *a , int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap_MAT_VAL_TYPE(MAT_VAL_TYPE *a , MAT_VAL_TYPE *b)
{
    MAT_VAL_TYPE tmp = *a;
    *a = *b;
    *b = tmp;
}

int choose_pivot(int i, int j)
{
    return (i+j)/2;
}

// template<typename iT, typename vT>
void quicksort_keyval_int_MAT_VAL_TYPE(int *key, MAT_VAL_TYPE *val, int start, int end)
{
    int pivot;
    int i, j, k;

    if (start < end)
    {
        k = choose_pivot(start, end);
        swap_int(&key[start], &key[k]);
        swap_MAT_VAL_TYPE(&val[start], &val[k]);
        pivot = key[start];

        i = start + 1;
        j = end;
        while (i <= j)
        {
            while ((i <= end) && (key[i] <= pivot))
                i++;
            while ((j >= start) && (key[j] > pivot))
                j--;
            if (i < j)
            {
                swap_int(&key[i], &key[j]);
                swap_MAT_VAL_TYPE(&val[i], &val[j]);
            }
        }

        // swap two elements
        swap_int(&key[start], &key[j]);
        swap_MAT_VAL_TYPE(&val[start], &val[j]);
 
        // recursively sort the lesser key
        quicksort_keyval_int_MAT_VAL_TYPE(key, val, start, j-1);
        quicksort_keyval_int_MAT_VAL_TYPE(key, val, j+1, end);
    }
}

void quicksort_keyval_int_int(int *key, int *val, int start, int end)
{
    int pivot;
    int i, j, k;

    if (start < end)
    {
        k = choose_pivot(start, end);
        swap_int(&key[start], &key[k]);
        swap_int(&val[start], &val[k]);
        pivot = key[start];

        i = start + 1;
        j = end;
        while (i <= j)
        {
            while ((i <= end) && (key[i] <= pivot))
                i++;
            while ((j >= start) && (key[j] > pivot))
                j--;
            if (i < j)
            {
                swap_int(&key[i], &key[j]);
                swap_int(&val[i], &val[j]);
            }
        }

        // swap two elements
        swap_int(&key[start], &key[j]);
        swap_int(&val[start], &val[j]);
 
        // recursively sort the lesser key
        quicksort_keyval_int_int(key, val, start, j-1);
        quicksort_keyval_int_int(key, val, j+1, end);
    }
}

void levelset_reordering_colrow_csc(const int           *cscColPtrTR,
                         const int           *cscRowIdxTR,
                         const MAT_VAL_TYPE    *cscValTR,
                               int           *cscColPtrTR_new,
                               int           *cscRowIdxTR_new,
                               MAT_VAL_TYPE    *cscValTR_new,
                               int           *levelItem,
                               int            m,
                               int            n,
                               int            nnzTR,
                               int            substitution)
{
    // transpose to have csr data
    int *csrRowPtrTR = (int *)malloc((m+1) * sizeof(int));
    
    // transpose from csc to csr
    matrix_transposition_litelite(m, n, nnzTR,
                         cscColPtrTR, cscRowIdxTR, csrRowPtrTR);

    int  *levelPtr  = (int *)malloc((m+1) * sizeof(int));

    int nlv = 0;
    findlevel(cscColPtrTR, cscRowIdxTR, csrRowPtrTR, m, &nlv, levelPtr, levelItem);
    // printf("nlv = %d\n", nlv);
    // printf("levelPtr:\n");
    // for (int i = 0; i <= nlv; i++)
    // printf("%d ", levelPtr[i]);
    // printf("\n");
    // printf("levelItem:\n");
    // for (int i = 0; i < m; i++)
    // printf("%d ", levelItem[i]);
    // printf("\n");
    // for (int i = 0;  i <= nlv; i++)
    // printf("%d ", levelPtr[i]);
    // printf("\n");
    // for (int i = 0; i < m; i++)
    // printf("%d ", levelItem[i]);
    // printf("\n");


    int  *levelItem_tmp  = (int *)malloc(m * sizeof(int));
    memcpy(levelItem_tmp, levelItem, m * sizeof(int));
    int  *levelperm  = (int *)malloc(m * sizeof(int));


    if (substitution == SUBSTITUTION_FORWARD)
    for (int i = 0; i < m; i++)
        levelperm[i] = i;
    else
    for (int i = m - 1; i >= 0; i--)
        levelperm[i] = i;

    //quick_sort_key_val_pair<int, int>(levelItem_tmp, levelperm, m);
    // if (substitution == SUBSTITUTION_FORWARD)
    quicksort_keyval_int_int(levelItem_tmp, levelperm, 0, m-1);
    // else
    // quicksort_keyval<int, int>(levelItem_tmp, levelperm, m - 1, 0);

    // ------------------by zhengyang-------------------
    if (substitution == SUBSTITUTION_BACKWARD)
    {
        for (int i = 0; i < m; i++)
        {
            levelperm[levelItem[i]] = m - i - 1;
        }
    }
    // --------------------------------------------------

    // printf("levelperm:\n");
    // for (int i = 0; i < m; i++)
    // printf("%d ", levelperm[i]);
    // printf("\n");

    // reorder columns
    cscColPtrTR_new[0] = 0;
    
    for (int i = 0; i < n; i++)
    {
        int idx = substitution == SUBSTITUTION_FORWARD ? levelItem[i] : levelItem[n - i - 1];
        // int idx = levelItem[i];

        int nnzr = cscColPtrTR[idx+1] - cscColPtrTR[idx]; 
        // cscColPtrTR_new[n - i] = cscColPtrTR_new[n - i - 1] + nnzr;
        cscColPtrTR_new[i+1] = cscColPtrTR_new[i] + nnzr;
        // printf("cscNew:\n");
        // printf("%d ", cscColPtrTR_new[i+1]);
        // printf("OK\n");
        // printf("idx = %d\n", idx);
        for (int j = 0; j < nnzr; j++)
        {
            // int off = substitution == SUBSTITUTION_FORWARD ? cscColPtrTR[idx] + j : cscColPtrTR[idx + 1] - j;
            int off = cscColPtrTR[idx] + j;
            int off_new = cscColPtrTR_new[i] + j;
            cscRowIdxTR_new[off_new] = cscRowIdxTR[off];
            // printf("%d %d    ", off, cscRowIdxTR_new[off_new]);
            cscValTR_new[off_new] = cscValTR[off];
        }
        // printf("\n");
    }

    // reorder row ids in each column
    for (int i = 0; i < nnzTR; i++)
    {
        // int val = substitution == SUBSTITUTION_FORWARD ? 
        //                 cscRowIdxTR_new[i] : m - cscRowIdxTR_new[i];

        // adjust---------------------------------------------------
        // if (substitution == SUBSTITUTION_FORWARD)
        // cscRowIdxTR_new[i] = levelperm[cscRowIdxTR_new[i]];
        // else
        // cscRowIdxTR_new[i] = m - levelperm[cscRowIdxTR_new[i]] - 1;
        // --------------------------------------------------------

        cscRowIdxTR_new[i] = levelperm[cscRowIdxTR_new[i]];
    }

    // -----------------------by zhengyang---------------------------
    if (substitution == SUBSTITUTION_BACKWARD)
    {
        int *levelItem_tmp = (int *)malloc(sizeof(int) * m);
        for (int i = 0; i < m; i ++)
        {
            levelItem_tmp[m - i - 1] = levelItem[i];
        }
        memcpy(levelItem, levelItem_tmp, sizeof(int) * m);

        // printf("levelItem new:\n");
        // for (int i = 0; i < m; i++)
        // printf("%d ", levelItem[i]);
        // printf("\n");
    }    
    // --------------------------------------------------------------

    free(csrRowPtrTR);
    free(levelPtr);
    free(levelperm);
    free(levelItem_tmp);

    return;
}

void levelset_reordering_vecb(const MAT_VAL_TYPE    *b, 
                             const MAT_VAL_TYPE    *x_ref, 
                                   MAT_VAL_TYPE    *b_perm, 
                                   MAT_VAL_TYPE    *x_ref_perm, 
                             const int           *levelItem, 
                             const int            m)
{
    int  *levelItem_tmp  = (int *)malloc(m * sizeof(int));
    memcpy(levelItem_tmp, levelItem, m * sizeof(int));
    int  *levelperm  = (int *)malloc(m * sizeof(int));
    for (int i = 0; i < m; i++)
        levelperm[i] = i;

    //quick_sort_key_val_pair<int, int>(levelItem_tmp, levelperm, m);
    quicksort_keyval_int_int(levelItem_tmp, levelperm, 0, m-1);

    for (int i = 0; i < m; i++)
    {
        b_perm[i] = b[levelItem[i]];
        x_ref_perm[i] = x_ref[levelItem[i]];
    }

    free(levelperm);
    free(levelItem_tmp);

    return;
}


#endif
