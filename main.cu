#include "common.h"
#include "mmio_highlevel.h"
#include "utils.h"
#include "csr2tile.h"
#include "external/CSR5_cuda/anonymouslib_cuda.h"
#include "tilespmv_cpu.h"
#include "tilespmv_cuda.h"
#include "cusparseSpSV.h"
#include "sptrsv_syncfree_cuda.h"
#include "tilesptrsv_syncfree.h"

# define INDEX_DATA_TYPE unsigned char
#define DEBUG_FORMATCOST 0

int main(int argc, char ** argv)
{

	if (argc < 2)
    {
        printf("Run the code by './test -d 0 A.mtx'.\n");
        return 0;
    }
	
    printf("--------------------------------!!!!!!!!------------------------------------\n");

 	struct timeval t1, t2;
	int rowA;
	int colA;
	MAT_PTR_TYPE nnzA;
	int isSymmetricA;
    MAT_VAL_TYPE *csrValA;
    int *csrColIdxA;
    MAT_PTR_TYPE *csrRowPtrA;
	
    int device_id = 0;
    // "Usage: ``./test -d 0 A.mtx'' for Ax=y on device 0"
    int argi = 1;

    // load device id
    char *devstr;
    if(argc > argi)
    {
        devstr = argv[argi];
        argi++;
    }

    if (strcmp(devstr, "-d") != 0) return 0;

    if(argc > argi)
    {
        device_id = atoi(argv[argi]);
        argi++;
    }
    printf("device_id = %i\n", device_id);


	char  *filename;
    filename = argv[3];
    printf("MAT: -------------- %s --------------\n", filename);

    // load mtx A data to the csr format
    gettimeofday(&t1, NULL);
    mmio_allinone(&rowA, &colA, &nnzA, &isSymmetricA, &csrRowPtrA, &csrColIdxA, &csrValA, filename);
    gettimeofday(&t2, NULL);
    double time_loadmat  = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    printf("  input matrix A: ( %i, %i ) nnz = %i\n  loadfile time    = %4.5f sec\n", rowA, colA, nnzA, time_loadmat/1000.0);

	for (int i = 0; i < nnzA; i++)
	    csrValA[i] = rand() % 10 + 1;

    rowA = (rowA / BLOCK_SIZE) * BLOCK_SIZE;
    colA = rowA;

    int *csrRowPtrTR = (int *)malloc((rowA + 1) * sizeof(int));
    int *csrColIdxTR = (int *)malloc((rowA + nnzA) * sizeof(int));
    MAT_VAL_TYPE *csrValTR = (MAT_VAL_TYPE *)malloc((rowA + nnzA) * sizeof(MAT_VAL_TYPE));
    // extract L or U with a unit diagonal of A
    int nnzTR;
    {
        int nnz_pointer = 0;
        csrRowPtrTR[0] = 0;
        for (int i = 0; i < rowA; i++)
        {
            for (int j = csrRowPtrA[i]; j < csrRowPtrA[i + 1]; j++)
            {
                if (csrColIdxA[j] < i)
                {
                    csrColIdxTR[nnz_pointer] = csrColIdxA[j];
                    csrValTR[nnz_pointer] = csrValA[j];
                    nnz_pointer++;
                }
            }
            // add dia nonzero
            csrColIdxTR[nnz_pointer] = i;
            csrValTR[nnz_pointer] = 1;
            nnz_pointer++;
            csrRowPtrTR[i + 1] = nnz_pointer;
        }
        int nnz_tmp_L = csrRowPtrTR[rowA];
        int nnzL = nnz_tmp_L;
        csrColIdxTR = (int *)realloc(csrColIdxTR, sizeof(int) * nnzL);
        csrValTR = (MAT_VAL_TYPE *)realloc(csrValTR, sizeof(MAT_VAL_TYPE) * nnzL);
        nnzTR = nnzL;
        nnzA = nnzL;
    }
    csrColIdxTR = (int *)realloc(csrColIdxTR, sizeof(int) * nnzTR);
    csrValTR = (MAT_VAL_TYPE *)realloc(csrValTR, sizeof(MAT_VAL_TYPE) * nnzTR);
    
    free(csrValA);
    free(csrColIdxA);
    free(csrRowPtrA);


    // set device
    cudaSetDevice(device_id);
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device_id);

    printf("---------------------------------------------------------------------------------------------\n");
    printf("Device [ %i ] %s @ %4.2f MHz\n", device_id, deviceProp.name, deviceProp.clockRate * 1e-3f);

    MAT_VAL_TYPE *x_ref = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * colA);
	MAT_VAL_TYPE *x = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * colA);
    memset(x, 0, sizeof(MAT_VAL_TYPE) * colA);
	for (int i = 0; i < colA; i++)
	{
		x_ref[i] = rand() % 10 + 1;
	}
    
    // compute reference results on a cpu core
	MAT_VAL_TYPE *b = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * rowA);
	for (int i = 0; i < rowA; i++)
	{
		MAT_VAL_TYPE sum = 0;
		for (int j = csrRowPtrTR[i]; j < csrRowPtrTR[i+1]; j++)
		{
			sum += csrValTR[j] * x_ref[csrColIdxTR[j]];
		}
		b[i] = sum;
	}

    int *cscRowIdxTR = (int *)malloc(nnzTR * sizeof(int));
    int *cscColPtrTR = (int *)malloc((colA+1) * sizeof(int));
    memset(cscColPtrTR, 0, (colA+1) * sizeof(int));
    MAT_VAL_TYPE *cscValTR    = (MAT_VAL_TYPE *)malloc(nnzTR * sizeof(MAT_VAL_TYPE));
    // transpose from csr to csc
    matrix_transposition(rowA, colA, nnzTR,
                         csrRowPtrTR, csrColIdxTR, csrValTR,
                         cscRowIdxTR, cscColPtrTR, cscValTR);
    
    
    // reorder
    int *levelItem_reorder = (int *)malloc(sizeof(int) * rowA);
    int *cscColPtr_new = (int *)malloc(sizeof(int) * (colA + 1)); 
    cscColPtr_new[0] = 0;
    int *cscRowIdx_new = (int *)malloc(sizeof(int) * nnzTR); 
    MAT_VAL_TYPE *cscVal_new = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * nnzTR);
    levelset_reordering_colrow_csc(cscColPtrTR, cscRowIdxTR, cscValTR, 
                                    cscColPtr_new, cscRowIdx_new, cscVal_new, 
                                    levelItem_reorder, rowA, colA, nnzTR, 0);
    int *csrColIdx_new = (int *)malloc(nnzTR * sizeof(int));
    int *csrRowPtr_new = (int *)malloc((rowA+1) * sizeof(int));
    memset(csrRowPtr_new, 0, (rowA+1) * sizeof(int));
    MAT_VAL_TYPE *csrVal_new = (MAT_VAL_TYPE *)malloc(nnzTR * sizeof(MAT_VAL_TYPE));
    matrix_transposition(rowA, colA, nnzTR,
                            cscColPtr_new, cscRowIdx_new, cscVal_new,
                            csrColIdx_new, csrRowPtr_new, csrVal_new);
    memcpy(csrRowPtrTR, csrRowPtr_new, sizeof(int) * (rowA+1));
    memcpy(csrColIdxTR, csrColIdx_new, sizeof(int) * (nnzTR));
    memcpy(csrValTR, csrVal_new, sizeof(MAT_VAL_TYPE) * (nnzTR));
    free(csrRowPtr_new);
    free(csrColIdx_new);
    free(csrVal_new);
    free(cscRowIdxTR);
    free(cscColPtrTR);
    free(cscValTR);
    
    MAT_VAL_TYPE *b_tmp = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * colA);
    MAT_VAL_TYPE *x_ref_tmp = (MAT_VAL_TYPE *)malloc(sizeof(MAT_VAL_TYPE) * rowA);
    levelset_reordering_vecb(b, x_ref, b_tmp, x_ref_tmp, levelItem_reorder, rowA);
    for (int i = 0; i < rowA; i++)
    {
        b[i] = b_tmp[i];
        x_ref[i] = x_ref_tmp[i];
    }
    free(levelItem_reorder);
    free(b_tmp);
    free(x_ref_tmp);


    Tile_matrix *matrixA = (Tile_matrix *)malloc(sizeof (Tile_matrix));

    double time_conversion = 0;
    long long tile_malloc_size = 0;

    //format conversion
    Tile_create(matrixA, 
                rowA, colA, nnzTR,
                csrRowPtrTR,
                csrColIdxTR,
                csrValTR,
                cscColPtr_new,
                cscRowIdx_new,
                cscVal_new,
                &time_conversion,
                &tile_malloc_size);
    int tilenum = matrixA->tilenum;


    int * ptroffset1 = (int *)malloc(sizeof(int) * tilenum);
    int * ptroffset2 = (int *)malloc(sizeof(int) * tilenum);
    memset(ptroffset1, 0, sizeof(int) * tilenum);
    memset(ptroffset2, 0, sizeof(int) * tilenum);

    int rowblkblock = 0;

    unsigned int * blkcoostylerowidx ;
    int * blkcoostylerowidx_colstart   ;
    int * blkcoostylerowidx_colstop ;
    int *multicoo_ptr = (int *)malloc((rowA + 1) * sizeof(int));

    int *multicoo_colidx ;
    MAT_VAL_TYPE *multicoo_val ;

    tilespmv_cpu(matrixA,
                ptroffset1,
                ptroffset2,
                &rowblkblock,
                &blkcoostylerowidx,
                &blkcoostylerowidx_colstart,
                &blkcoostylerowidx_colstop,
                rowA, colA, nnzTR,
                csrRowPtrTR,
                csrColIdxTR,
                csrValTR,
                x,
                b,
                x_ref
            );

    double alpha = 1.0;
    //run GPU Tilesptrsv
    memset(x, 0, sizeof(MAT_VAL_TYPE) * rowA);
    call_tilesptrsv_cuda( filename,
                            matrixA,
                            ptroffset1,
                            ptroffset2,
                            rowblkblock,
                            blkcoostylerowidx,
                            blkcoostylerowidx_colstart,
                            blkcoostylerowidx_colstop,
                            rowA, colA, nnzA,
                            csrRowPtrA,
                            csrColIdxA,
                            csrValA,
                            alpha,
                            x,
                            b,
                            x_ref,
                            &time_conversion,
                            &tile_malloc_size);

    free(matrixA);
}
