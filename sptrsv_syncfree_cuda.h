#ifndef _SPTRSV_SYNCFREE_CUDA_
#define _SPTRSV_SYNCFREE_CUDA_

#include "common.h"
// #include "utils.h"
#include <cuda_runtime.h>
// # define MAT_VAL_TYPE double
# define SUBSTITUTION_FORWARD 0
#define OPT_WARP_NNZ   1
#define OPT_WARP_RHS   2
#define OPT_WARP_AUTO  3

__global__
void sptrsv_syncfree_cuda_analyser(const int   *d_cscRowIdx,
                                   const int    m,
                                   const int    nnz,
                                         int   *d_graphInDegree)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x; //get_global_id(0);
    if (global_id < nnz)
    {
        atomicAdd(&d_graphInDegree[d_cscRowIdx[global_id]], 1);
    }
}


__global__
void sptrsv_syncfree_cuda_executor_update(const int*         d_cscColPtr,
                                          const int*         d_cscRowIdx,
                                          const MAT_VAL_TYPE*  d_cscVal,
                                          int*                           d_graphInDegree,
                                          MAT_VAL_TYPE*                    d_left_sum,
                                          const int                      m,
                                          const int                      substitution,
                                          const MAT_VAL_TYPE*  d_b,
                                          MAT_VAL_TYPE*                    d_x,
                                          int*                           d_while_profiler,
                                          int*                           d_id_extractor)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x;

    // Initialize
    const int local_warp_id = threadIdx.x / WARP_SIZE;
    const int lane_id = (WARP_SIZE - 1) & threadIdx.x;
    int global_x_id = 0;
    if (!lane_id)
        global_x_id = atomicAdd(d_id_extractor, 1);
    global_x_id = __shfl_sync(0xffffffff, global_x_id, 0);

    if (global_x_id >= m) return;

    // substitution is forward or backward
    global_x_id = substitution == SUBSTITUTION_FORWARD ? 
                  global_x_id : m - 1 - global_x_id;
    
    // Prefetch
    const int pos = substitution == SUBSTITUTION_FORWARD ? 
                    d_cscColPtr[global_x_id] : d_cscColPtr[global_x_id+1]-1;
    const MAT_VAL_TYPE coef = (MAT_VAL_TYPE)1 / d_cscVal[pos];
    //asm("prefetch.global.L2 [%0];"::"d"(d_cscVal[d_cscColPtr[global_x_id] + 1 + lane_id]));
    //asm("prefetch.global.L2 [%0];"::"r"(d_cscRowIdx[d_cscColPtr[global_x_id] + 1 + lane_id]));

    // Consumer
    do {
        __threadfence_block();
    }
    while (d_graphInDegree[global_x_id] != 1);

    MAT_VAL_TYPE xi = d_left_sum[global_x_id];
    xi = (d_b[global_x_id] - xi) * coef;

    // Producer
    const int start_ptr = substitution == SUBSTITUTION_FORWARD ? 
                          d_cscColPtr[global_x_id]+1 : d_cscColPtr[global_x_id];
    const int stop_ptr  = substitution == SUBSTITUTION_FORWARD ? 
                          d_cscColPtr[global_x_id+1] : d_cscColPtr[global_x_id+1]-1;
    for (int jj = start_ptr + lane_id; jj < stop_ptr; jj += WARP_SIZE)
    {
        const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
        const int rowIdx = d_cscRowIdx[j];

        atomicAdd(&d_left_sum[rowIdx], xi * d_cscVal[j]);
        __threadfence();
        atomicSub(&d_graphInDegree[rowIdx], 1);
    }

    //finish
    if (!lane_id) d_x[global_x_id] = xi;
}


__global__
void sptrsm_syncfree_cuda_executor_update(const int* __restrict__        d_cscColPtr,
                                          const int* __restrict__        d_cscRowIdx,
                                          const MAT_VAL_TYPE* __restrict__ d_cscVal,
                                          int*                           d_graphInDegree,
                                          MAT_VAL_TYPE*                    d_left_sum,
                                          const int                      m,
                                          const int                      substitution,
                                          const int                      rhs,
                                          const int                      opt,
                                          const MAT_VAL_TYPE* __restrict__ d_b,
                                          MAT_VAL_TYPE*                    d_x,
                                          int*                           d_while_profiler,
                                          int*                           d_id_extractor)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x;

    // Initialize
    const int local_warp_id = threadIdx.x / WARP_SIZE;
    const int lane_id = (WARP_SIZE - 1) & threadIdx.x;
    int global_x_id = 0;
    if (!lane_id)
        global_x_id = atomicAdd(d_id_extractor, 1);
    global_x_id = __shfl_sync(0xffffffff, global_x_id, 0);

    if (global_x_id >= m) return;

    // substitution is forward or backward
    global_x_id = substitution == SUBSTITUTION_FORWARD ? 
                  global_x_id : m - 1 - global_x_id;

    // Prefetch
    const int pos = substitution == SUBSTITUTION_FORWARD ?
                d_cscColPtr[global_x_id] : d_cscColPtr[global_x_id+1]-1;
    const MAT_VAL_TYPE coef = (MAT_VAL_TYPE)1 / d_cscVal[pos];
    //asm("prefetch.global.L2 [%0];"::"d"(d_cscVal[d_cscColPtr[global_x_id] + 1 + lane_id]));
    //asm("prefetch.global.L2 [%0];"::"r"(d_cscRowIdx[d_cscColPtr[global_x_id] + 1 + lane_id]));

    // Consumer
    do {
        __threadfence_block();
    }
    while (1 != d_graphInDegree[global_x_id]);
  
    //// Consumer
    //int graphInDegree;
    //do {
    //    //bypass Tex cache and avoid other mem optimization by nvcc/ptxas
    //    asm("ld.global.u32 %0, [%1];" : "=r"(graphInDegree),"=r"(d_graphInDegree[global_x_id]) :: "memory"); 
    //}
    //while (1 != graphInDegree );

    for (int k = lane_id; k < rhs; k += WARP_SIZE)
    {
        const int pos = global_x_id * rhs + k;
        d_x[pos] = (d_b[pos] - d_left_sum[pos]) * coef;
    }

    // Producer
    const int start_ptr = substitution == SUBSTITUTION_FORWARD ? 
                          d_cscColPtr[global_x_id]+1 : d_cscColPtr[global_x_id];
    const int stop_ptr  = substitution == SUBSTITUTION_FORWARD ? 
                          d_cscColPtr[global_x_id+1] : d_cscColPtr[global_x_id+1]-1;

    if (opt == OPT_WARP_NNZ)
    {
        for (int jj = start_ptr + lane_id; jj < stop_ptr; jj += WARP_SIZE)
        {
            const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
            const int rowIdx = d_cscRowIdx[j];
            for (int k = 0; k < rhs; k++)
                atomicAdd(&d_left_sum[rowIdx * rhs + k], d_x[global_x_id * rhs + k] * d_cscVal[j]);
            __threadfence();
            atomicSub(&d_graphInDegree[rowIdx], 1);
        }
    }
    else if (opt == OPT_WARP_RHS)
    {
        for (int jj = start_ptr; jj < stop_ptr; jj++)
        {
            const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
            const int rowIdx = d_cscRowIdx[j];
            for (int k = lane_id; k < rhs; k+=WARP_SIZE)
                atomicAdd(&d_left_sum[rowIdx * rhs + k], d_x[global_x_id * rhs + k] * d_cscVal[j]);
            __threadfence();
            if (!lane_id) atomicSub(&d_graphInDegree[rowIdx], 1);
        }
    }
    else if (opt == OPT_WARP_AUTO)
    {
        const int len = stop_ptr - start_ptr;

        if ((len <= rhs || rhs > 16) && len < 2048)
        {
            for (int jj = start_ptr; jj < stop_ptr; jj++)
            {
                const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
                const int rowIdx = d_cscRowIdx[j];
                for (int k = lane_id; k < rhs; k+=WARP_SIZE)
                    atomicAdd(&d_left_sum[rowIdx * rhs + k], d_x[global_x_id * rhs + k] * d_cscVal[j]);
                __threadfence();
                if (!lane_id) atomicSub(&d_graphInDegree[rowIdx], 1);
            }
        }
        else
        {
            for (int jj = start_ptr + lane_id; jj < stop_ptr; jj += WARP_SIZE)
            {
                const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
                const int rowIdx = d_cscRowIdx[j];
                for (int k = 0; k < rhs; k++)
                    atomicAdd(&d_left_sum[rowIdx * rhs + k], d_x[global_x_id * rhs + k] * d_cscVal[j]);
                __threadfence();
                atomicSub(&d_graphInDegree[rowIdx], 1);
            }
        }
    }
}

int sptrsv_syncfree_cuda(const int           *cscColPtrTR,
                         const int           *cscRowIdxTR,
                         const MAT_VAL_TYPE    *cscValTR,
                         const int            m,
                         const int            n,
                         const int            nnzTR,
                         const int            substitution,
                         const int            rhs,
                         const int            opt,
                               MAT_VAL_TYPE    *x,
                         const MAT_VAL_TYPE    *b,
                         const MAT_VAL_TYPE    *x_ref,
                         char                *filename)
{
    if (m != n)
    {
        printf("This is not a square matrix, return.\n");
        return -1;
    }

    // transfer host mem to device mem
    int *d_cscColPtrTR;
    int *d_cscRowIdxTR;
    MAT_VAL_TYPE *d_cscValTR;
    MAT_VAL_TYPE *d_b;
    MAT_VAL_TYPE *d_x;

    // Matrix L
    cudaMalloc((void **)&d_cscColPtrTR, (n+1) * sizeof(int));
    cudaMalloc((void **)&d_cscRowIdxTR, nnzTR  * sizeof(int));
    cudaMalloc((void **)&d_cscValTR,    nnzTR  * sizeof(MAT_VAL_TYPE));

    cudaMemcpy(d_cscColPtrTR, cscColPtrTR, (n+1) * sizeof(int),   cudaMemcpyHostToDevice);
    cudaMemcpy(d_cscRowIdxTR, cscRowIdxTR, nnzTR  * sizeof(int),   cudaMemcpyHostToDevice);
    cudaMemcpy(d_cscValTR,    cscValTR,    nnzTR  * sizeof(MAT_VAL_TYPE),   cudaMemcpyHostToDevice);

    // Vector b
    cudaMalloc((void **)&d_b, m * rhs * sizeof(MAT_VAL_TYPE));
    cudaMemcpy(d_b, b, m * rhs * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);

    // Vector x
    cudaMalloc((void **)&d_x, n * rhs * sizeof(MAT_VAL_TYPE));
    cudaMemset(d_x, 0, n * rhs * sizeof(MAT_VAL_TYPE));

    //  - cuda syncfree SpTRSV analysis start!
    printf(" - cuda syncfree SpTRSV analysis start!\n");

    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    // malloc tmp memory to generate in-degree
    int *d_graphInDegree;
    int *d_graphInDegree_backup;
    cudaMalloc((void **)&d_graphInDegree, m * sizeof(int));
    cudaMalloc((void **)&d_graphInDegree_backup, m * sizeof(int));

    int *d_id_extractor;
    cudaMalloc((void **)&d_id_extractor, sizeof(int));

    int num_threads = 128;
    int num_blocks = ceil ((double)nnzTR / (double)num_threads);

    for (int i = 0; i < 1; i++)
    {
        cudaMemset(d_graphInDegree, 0, m * sizeof(int));
        sptrsv_syncfree_cuda_analyser<<< num_blocks, num_threads >>>
                                      (d_cscRowIdxTR, m, nnzTR, d_graphInDegree);
    }
    cudaDeviceSynchronize();

    gettimeofday(&t2, NULL);
    double time_cuda_analysis = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    time_cuda_analysis /= 1;
    // *preprocessing_syncfree = time_cuda_analysis;

    printf("cuda syncfree SpTRSV analysis on L used %4.2f ms\n", time_cuda_analysis);

    //  - cuda syncfree SpTRSV solve start!
    printf(" - cuda syncfree SpTRSV solve start!\n");

    // malloc tmp memory to collect a partial sum of each row
    MAT_VAL_TYPE *d_left_sum;
    cudaMalloc((void **)&d_left_sum, sizeof(MAT_VAL_TYPE) * m * rhs);

    // backup in-degree array, only used for benchmarking multiple runs
    cudaMemcpy(d_graphInDegree_backup, d_graphInDegree, m * sizeof(int), cudaMemcpyDeviceToDevice);

    // this is for profiling while loop only
    int *d_while_profiler;
    cudaMalloc((void **)&d_while_profiler, sizeof(int) * n);
    cudaMemset(d_while_profiler, 0, sizeof(int) * n);
    int *while_profiler = (int *)malloc(sizeof(int) * n);

    for (int i = 0; i < WARMUP_NUM; i++)
    {
        // get a unmodified in-degree array, only for benchmarking use
        cudaMemcpy(d_graphInDegree, d_graphInDegree_backup, m * sizeof(int), cudaMemcpyDeviceToDevice);
        //cudaMemset(d_graphInDegree, 0, sizeof(int) * m);
        
        // clear left_sum array, only for benchmarking use
        cudaMemset(d_left_sum, 0, sizeof(MAT_VAL_TYPE) * m * rhs);
        cudaMemset(d_x, 0, sizeof(MAT_VAL_TYPE) * n * rhs);
        cudaMemset(d_id_extractor, 0, sizeof(int));

        if (rhs == 1)
        {
            num_threads = WARP_PER_BLOCK * WARP_SIZE;
            //num_threads = 1 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            // printf(".....\n");
            //sptrsv_syncfree_cuda_executor<<< num_blocks, num_threads >>>
            sptrsv_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, d_b, d_x, d_while_profiler, d_id_extractor);
            // printf("yyyyyy\n");
        }
        else
        {
            num_threads = 4 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            sptrsm_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, rhs, opt,
                                          d_b, d_x, d_while_profiler, d_id_extractor);
        }
        cudaDeviceSynchronize();
    }    

    for (int i = 0; i < WARMUP_NUM; i++)
    {
        // get a unmodified in-degree array, only for benchmarking use
        cudaMemcpy(d_graphInDegree, d_graphInDegree_backup, m * sizeof(int), cudaMemcpyDeviceToDevice);
        //cudaMemset(d_graphInDegree, 0, sizeof(int) * m);
        
        // clear left_sum array, only for benchmarking use
        cudaMemset(d_left_sum, 0, sizeof(MAT_VAL_TYPE) * m * rhs);
        cudaMemset(d_x, 0, sizeof(MAT_VAL_TYPE) * n * rhs);
        cudaMemset(d_id_extractor, 0, sizeof(int));

        if (rhs == 1)
        {
            num_threads = WARP_PER_BLOCK * WARP_SIZE;
            //num_threads = 1 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            //sptrsv_syncfree_cuda_executor<<< num_blocks, num_threads >>>
            sptrsv_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, d_b, d_x, d_while_profiler, d_id_extractor);
        }
        else
        {
            num_threads = 4 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            sptrsm_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, rhs, opt,
                                          d_b, d_x, d_while_profiler, d_id_extractor);
        }

        cudaDeviceSynchronize();
    }

    // step 5: solve L*y = x
    double time_cuda_solve = 0;
    for (int i = 0; i < BENCH_REPEAT; i++)
    {
        // get a unmodified in-degree array, only for benchmarking use
        cudaMemcpy(d_graphInDegree, d_graphInDegree_backup, m * sizeof(int), cudaMemcpyDeviceToDevice);
        
        // clear left_sum array, only for benchmarking use
        cudaMemset(d_left_sum, 0, sizeof(MAT_VAL_TYPE) * m * rhs);
        cudaMemset(d_x, 0, sizeof(MAT_VAL_TYPE) * n * rhs);
        cudaMemset(d_id_extractor, 0, sizeof(int));

        gettimeofday(&t1, NULL);

        if (rhs == 1)
        {
            num_threads = WARP_PER_BLOCK * WARP_SIZE;
            //num_threads = 1 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            //sptrsv_syncfree_cuda_executor<<< num_blocks, num_threads >>>
            sptrsv_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, d_b, d_x, d_while_profiler, d_id_extractor);
        }
        else
        {
            num_threads = 4 * WARP_SIZE;
            num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
            sptrsm_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          m, substitution, rhs, opt,
                                          d_b, d_x, d_while_profiler, d_id_extractor);
        }

        cudaDeviceSynchronize();
        gettimeofday(&t2, NULL);

        time_cuda_solve += (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    }

    time_cuda_solve /= BENCH_REPEAT;
    double flop = 2*(double)rhs*(double)nnzTR;

    printf("cuda syncfree SpTRSV solve used %4.2f ms, throughput is %4.2f gflops\n",
           time_cuda_solve, flop/(1e6*time_cuda_solve));
    double gflops = flop/(1e6*time_cuda_solve);

    // int level_fake = 0;
    // long long  malloc_size = (n+1) * sizeof(int) + nnzTR  * sizeof(int) + nnzTR  * sizeof(double);
    // FILE *fout = fopen("res/4090/syncfree.csv", "a");
    // fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f",
    //         m, n, nnzTR, level_fake, malloc_size, time_cuda_analysis, time_cuda_solve, gflops);
    // fclose(fout);
    // FILE *fout = fopen("syncfree.csv", "a");
    // if (fout == NULL)
    //     printf("Writing results fails.\n");
    // fprintf(fout, "%s,%i,%i,%i,%f,%f\n",
    //         filename, m, n, nnzTR, time_cuda_solve, gflops);
    // fclose(fout);

    cudaMemcpy(x, d_x, n * rhs * sizeof(MAT_VAL_TYPE), cudaMemcpyDeviceToHost);

    // validate x
    double accuracy = 1e-4;
    double ref = 0.0;
    double res = 0.0;

    for (int i = 0; i < n * rhs; i++)
    {
        ref += abs(x_ref[i]);
        res += abs(x[i] - x_ref[i]);
        //if (x_ref[i] != x[i]) printf ("[%i, %i] x_ref = %f, x = %f\n", i/rhs, i%rhs, x_ref[i], x[i]);
    }
    res = ref == 0 ? res : res / ref;

    if (res < accuracy)
        printf("cuda syncfree SpTRSV executor passed! |x-xref|/|xref| = %8.2e\n", res);
    else
        printf("cuda syncfree SpTRSV executor _NOT_ passed! |x-xref|/|xref| = %8.2e\n", res);

    int level_fake = 0;
    long long  malloc_size = (n+1) * sizeof(int) + nnzTR  * sizeof(int) + nnzTR  * sizeof(double);
    FILE *fout = fopen("res/4090/syncfree.csv", "a");
    fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f, %f",
            m, n, nnzTR, level_fake, malloc_size, time_cuda_analysis, time_cuda_solve, gflops, res);
    fclose(fout);


    // profile while loop
    cudaMemcpy(while_profiler, d_while_profiler, n * sizeof(int), cudaMemcpyDeviceToHost);
    long long unsigned int while_count = 0;
    for (int i = 0; i < n; i++)
    {
        while_count += while_profiler[i];
        //printf("while_profiler[%i] = %i\n", i, while_profiler[i]);
    }
    //printf("\nwhile_count= %llu in total, %llu per row/column\n", while_count, while_count/m);

    // step 6: free resources
    free(while_profiler);

    cudaFree(d_graphInDegree);
    cudaFree(d_graphInDegree_backup);
    cudaFree(d_id_extractor);
    cudaFree(d_left_sum);
    cudaFree(d_while_profiler);

    cudaFree(d_cscColPtrTR);
    cudaFree(d_cscRowIdxTR);
    cudaFree(d_cscValTR);
    cudaFree(d_b);
    cudaFree(d_x);

    return 0;
}

#endif



