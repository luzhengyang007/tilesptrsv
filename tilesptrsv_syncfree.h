#ifndef __TILESPTRSV_SYNCFREE__
#define __TILESPTRSV_SYNCFREE__

#include "common.h"
// #include "utils.h"
#include <cuda_runtime.h>
// # define MAT_VAL_TYPE double
# define SUBSTITUTION_FORWARD 0
#define OPT_WARP_NNZ   1
#define OPT_WARP_RHS   2
#define OPT_WARP_AUTO  3

__global__
void tilesptrsv_syncfree_cuda_analyser(const int   *d_cscRowIdx,
                                   const int    m,
                                   const int    nnz,
                                         int   *d_graphInDegree)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x; //get_global_id(0);
    if (global_id < nnz)
    {
        atomicAdd(&d_graphInDegree[d_cscRowIdx[global_id]], 1);
    }
}


__global__
void tilesptrsv_syncfree_cuda_executor_update(const int*         d_cscColPtr,
                                          const int*         d_cscRowIdx,
                                          const MAT_VAL_TYPE*  d_cscVal,
                                          int*                           d_graphInDegree,
                                          MAT_VAL_TYPE*                    d_left_sum,
                                          const int                      m,
                                          const int                      substitution,
                                          const MAT_VAL_TYPE*  d_b,
                                          MAT_VAL_TYPE*                    d_x,
                                          int*                           d_while_profiler,
                                          int*                           d_id_extractor,

                                          int*                           d_csctile_ptr,
                                          int*                           d_tile_rowidx,
                                          int*                           d_reflection,
                                          int*                           d_ptroffset1,
                                          int*                           d_ptroffset2,
                                          unsigned char*                           d_Blockcsr_Ptr,
                                          MAT_VAL_TYPE*                           d_Blockcsr_Val,
                                          unsigned char*                           d_blknnznnz,
                                          unsigned char*                           d_csr_compressedIdx,
                                          char*                          d_Format,
                                        
                                         unsigned char *d_coo_compressed_Idx,
                                         MAT_VAL_TYPE *d_Blockcoo_Val,
                                         MAT_VAL_TYPE *d_Blockdense_Val,
                                         int *d_dnsrowptr,
                                         MAT_VAL_TYPE *d_Blockdenserow_Val,
                                         char *d_denserowid,
                                         int *d_dnscolptr,
                                         MAT_VAL_TYPE *d_Blockdensecol_Val,
                                         char *d_densecolid,
                                          
                                         int *d_workgroup_colidx,
                                         int *d_colidx_workgroup_ptr,
                                         int *d_workgroup_startblkidx,
                                         int *d_workgroup_endblkidx,
                                          int workgroup_num,
                                          
                                          int *d_graphInDegree_diag)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x;

    // ----------------------- adjust -------------------------
    int workgroup_id = global_id >> 5;
    if (workgroup_id >= workgroup_num) return;
    int global_x_id = d_workgroup_colidx[workgroup_id];
    // --------------------------------------------------------

    // Initialize
    const int lane_id = (WARP_SIZE - 1) & threadIdx.x;
    // int global_x_id = 0;
    // if (!lane_id)
    //     global_x_id = atomicAdd(d_id_extractor, 1);
    // global_x_id = __shfl_sync(0xffffffff, global_x_id, 0);

    // if (workgroup_id >= workgroup_num) return;

    // substitution is forward or backward
    global_x_id = substitution == SUBSTITUTION_FORWARD ? 
                  global_x_id : m - 1 - global_x_id;
    
    // int first_workgroup_thiscol = d_colidx_workgroup_ptr[global_x_id];

    // Consumer
    do {
        __threadfence_block();
        // __syncwarp();
        // printf("tile degree = %d\n", d_graphInDegree[global_x_id]);
    }
    while (d_graphInDegree[global_x_id] != 1);

    int colblkid = global_x_id;
    int first_workgroup_thiscol = d_colidx_workgroup_ptr[global_x_id];
    
    int x_offset = colblkid * BLOCK_SIZE;


    // --------------------- sptrsv -------------------------
    // if (!lane_id)
    // {
    //     // int blkid = d_csctile_ptr[colblkid];
    //     // printf("work id = %d\n", workgroup_id);
    //     int blkid = d_workgroup_startblkidx[first_workgroup_thiscol];
    //     int rowblkid = d_tile_rowidx[blkid];
    //     int ref_blkid = d_reflection[blkid];
    //     int csroffset = d_ptroffset1[ref_blkid];
    //     int csrcount = d_ptroffset2[ref_blkid];
    //     int rowlength = BLOCK_SIZE;
    //     for (int ri = 0; ri < rowlength; ri++)
    //     {
    //         int start = d_Blockcsr_Ptr[csrcount + ri];
    //         int stop = ri == rowlength - 1 ? d_blknnznnz[ref_blkid] : d_Blockcsr_Ptr[ri + 1 + csrcount];
    //         double sum = 0;
    //         for (int rj = start; rj < stop - 1; rj++)
    //         {
    //             int csrcol = csroffset + rj;
    //             unsigned char csridx = d_csr_compressedIdx[csrcol >> 1];
    //             csrcol = csrcol % 2;
    //             csrcol = csrcol == 0 ? (csridx & num_f) >> 4 : csridx & num_b;
    //             sum += d_x[x_offset + csrcol] * d_Blockcsr_Val[csroffset + rj];                
    //         }
    //         d_x[rowblkid * BLOCK_SIZE + ri] = (d_b[rowblkid * BLOCK_SIZE + ri] - d_left_sum[rowblkid * BLOCK_SIZE + ri] - sum) / d_Blockcsr_Val[csroffset + stop - 1];
    //     }
    // }
    // __syncwarp();
    // --------------------------------------------------------

    // {
        // printf("tid = %d\n", global_id);
        const int local_warp_id = threadIdx.x / WARP_SIZE;
        // const int lane_id = (WARP_SIZE - 1) & threadIdx.x; // threadid in warp
        const int lane_lane_id = lane_id % 2;
        const int global_xx_id = lane_id / 2;

        int blkid = d_workgroup_startblkidx[first_workgroup_thiscol];
        int rowblkid = d_tile_rowidx[blkid];
        int ref_blkid = d_reflection[blkid];
        int csroffset = d_ptroffset1[ref_blkid];
        int csrcount = d_ptroffset2[ref_blkid];
        int rowlength = BLOCK_SIZE;

        __shared__ MAT_VAL_TYPE s_left_sum[WARP_PER_BLOCK * BLOCK_SIZE];
        MAT_VAL_TYPE *s_left_sum_warp = &s_left_sum[local_warp_id * BLOCK_SIZE];
        if (!lane_lane_id)
        {
            s_left_sum_warp[global_xx_id] = 0;
        }
        const int pos = d_Blockcsr_Ptr[csrcount + global_xx_id];
        const MAT_VAL_TYPE coef = d_Blockcsr_Val[csroffset + pos];
        int *d_graphInDegree_tmp = &d_graphInDegree_diag[rowblkid * BLOCK_SIZE];
        // if (!lane_lane_id)
        //     printf("%d ", d_graphInDegree_tmp)
        // printf("????\n");
        // for (int ii = 0;;ii++)
        // {
        //     if (d_graphInDegree_tmp[global_xx_id] == 1)
        //         break;
        //     printf("tid = %d    degree id = %d   degree = %d\n", global_id, global_xx_id, d_graphInDegree_tmp[global_xx_id]);
        //     __syncwarp();
        // }
        do {
            // do 
            // {
                __syncwarp();     
            //     printf("tid = %d    degree id = %d   degree = %d\n", global_id, global_xx_id, d_graphInDegree_tmp[global_xx_id]);
            // }
            // while (d_graphInDegree_tmp[global_xx_id] != 1);
            // __threadfence();
            // __syncthreads();
            // printf("tid = %d    degree id = %d   degree = %d\n", global_id, global_xx_id, d_graphInDegree_tmp[global_xx_id]);
        }
        while (d_graphInDegree_tmp[global_xx_id] != 1);

        // printf("tid = %d\n", global_id);

        MAT_VAL_TYPE xi = s_left_sum_warp[global_xx_id] + d_left_sum[rowblkid * BLOCK_SIZE + global_xx_id];
        xi = (d_b[rowblkid * BLOCK_SIZE + global_xx_id] - xi) / coef;
        const int start_ptr = d_Blockcsr_Ptr[csrcount + global_xx_id] + 1;
        const int stop_ptr  = global_xx_id == BLOCK_SIZE - 1 ?
                            d_blknnznnz[ref_blkid] : d_Blockcsr_Ptr[csrcount + global_xx_id + 1];
        for (int jj = start_ptr + lane_lane_id; jj < stop_ptr; jj += 2)
        {
            int cscrow = csroffset + jj;
            unsigned char cscidx = d_csr_compressedIdx[cscrow >> 1];
            cscrow = cscrow % 2;
            cscrow = cscrow == 0 ? (cscidx & num_f) >> 4 : cscidx & num_b;
            
            atomicAdd(&s_left_sum_warp[cscrow], xi * d_Blockcsr_Val[csroffset + jj]);
            // __threadfence();
            atomicSub(&d_graphInDegree_tmp[cscrow], 1);
            // __syncwarp();
            // printf("d_graphInDegree_tmp = %d\n", d_graphInDegree_tmp[cscrow]);
        }

        // __syncwarp();    
            printf("1 tid = %d\n", global_id);

        // if (!lane_id || lane_id == 7)
        //     for (int i = 0 ; i < BLOCK_SIZE; i++)
        //         printf("%d %d\n", i, d_graphInDegree_tmp[i]);                

        if (!lane_lane_id) 
        {
            printf("tid = %d id = %d x = %.1lf\n", global_id, x_offset + global_xx_id, xi);
            d_x[x_offset + global_xx_id] = xi;
        }

            printf("2 tid = %d\n", global_id);
    // }
    // for (int i = 0; i < BLOCK_SIZE; i++)
    //      __syncwarp();
        // __threadfence();
    // __syncwarp();
    // __syncwarp();
    // __syncwarp();
    // __syncthreads();
    // __threadfence_block();
        for (int i = 0; i < BLOCK_SIZE; i++)
            __syncwarp();
    // __threadfence_block();

    
printf("3 tid = %d\n", global_id);

int start = first_workgroup_thiscol == workgroup_id ? d_workgroup_startblkidx[workgroup_id] + 1 :
                                                      d_workgroup_startblkidx[workgroup_id];
if (!lane_id)
    printf("workid = %d     start = %d  end = %d\n", workgroup_id, start, d_workgroup_endblkidx[workgroup_id]);
for (int j = start; j < d_workgroup_endblkidx[workgroup_id]; j++)
        {
// printf("tid = %d\n", global_id);

            // printf("blkj = ??\n");
            int blkj = d_reflection[j];
            char subformat = d_Format[blkj];
            int colid = colblkid;
            int collength = BLOCK_SIZE;
            int rowoffset = d_tile_rowidx[j] * BLOCK_SIZE;
            int rowtileoffset = d_tile_rowidx[j];

                switch (subformat)
                {
                // if CSR
                case 0:
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int csroffset = d_ptroffset1[blkj];
                        int csrcount = d_ptroffset2[blkj];


                        int ri = lane_id >> 1;
                        int virtual_lane_id = lane_id & 0x1;

                        int stop = ri == BLOCK_SIZE - 1 ? d_blknnznnz[blkj] : d_Blockcsr_Ptr[ri + 1 + csrcount];

                        for (int rj = d_Blockcsr_Ptr[csrcount + ri] + virtual_lane_id; rj < stop; rj += 2)
                        {
                            int csrcol = csroffset + rj;
                            unsigned char csridx = d_csr_compressedIdx[csrcol >> 1];
                            csrcol = csrcol % 2;
                            csrcol = csrcol == 0 ? (csridx & num_f) >> 4 : csridx & num_b;
                            sum += d_x[x_offset + csrcol] * d_Blockcsr_Val[csroffset + rj];
                            // sum += s_x_warp[csrcol] * d_Blockcsr_Val[csroffset + rj];
                        }

                        sum += __shfl_down_sync(0xffffffff, sum, 1);
                        sumsum += __shfl_down_sync(0xffffffff, sum, lane_id);
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        // __syncwarp();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);

                    }

                    break;
                    
                case 1:
                    {
                        
                                        int coooffset = d_ptroffset1[blkj] + lane_id;

                                        int blknnz = d_blknnznnz[blkj];
                                        unsigned char aidx = lane_id < blknnz ? d_coo_compressed_Idx[coooffset] : 0;
                                        MAT_VAL_TYPE aval = lane_id < blknnz ? d_Blockcoo_Val[coooffset] : 0;

                                        if (lane_id < blknnz)
                                        {
                                            atomicAdd(&d_left_sum[rowoffset + ((aidx & num_f) >> 4)], aval * d_x[x_offset + (aidx & num_b)]);
                                            // atomicAdd(&d_left_sum[rowoffset + ((aidx & num_f) >> 4)], aval * s_x_warp[(aidx & num_b)]);
                                        }
                                            
                                        __threadfence();
                                        // __syncwarp();
                                        if (!lane_id)
                                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
//                 case 2:
//                     // if ELL
// #if DEBUG_FORMATCOST
//                     if (formatprofile == 2 || formatprofile == -1)
// #endif
//                     {
//                         sum = 0;
//                         int ellwoffset = d_ptroffset1[blkj - rowblkjstart];

//                         MAT_VAL_TYPE r_x = lane_id < collength ? d_x[x_offset + lane_id] : 0;

//                         int elllen = d_tilewidth[blkj] * BLOCK_SIZE;
//                         for (int rj = lane_id; rj < elllen; rj += WARP_SIZE)
//                         {
//                             int ellcol = ellwoffset + rj;
//                             unsigned int ellidx = d_ell_compressedIdx[ellcol >> 1];
//                             ellcol = ellcol % 2;
//                             ellcol = ellcol == 0 ? (ellidx & num_f) >> 4 : ellidx & num_b;
//                             MAT_VAL_TYPE r_x_gathered = lane_id < WARP_SIZE ? __shfl_sync(0x0000ffff, r_x, ellcol) : __shfl_sync(0xffff0000, r_x, ellcol);
//                             sum += d_Blockell_Val[ellwoffset + rj] * r_x_gathered;
//                         }
//                         sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

//                         sumsum += sum;
//                     }

//                     break;
//                 case 3:
//                     // if ELL

// #if DEBUG_FORMATCOST
//                     if (formatprofile == 3 || formatprofile == -1)
// #endif
//                     {
//                         // first do ELL (the above code can be called)
//                         sum = 0;

//                         int hybwoffset = s_ptroffset1_local[blkj - rowblkjstart];
//                         int hybidxoffset = s_ptroffset2_local[blkj - rowblkjstart];

//                         const MAT_VAL_TYPE r_x = lane_id < collength ? d_x[x_offset + lane_id] : 0;

//                         int elllen = d_tilewidth[blkj] * BLOCK_SIZE;
//                         for (int rj = lane_id; rj < elllen; rj += WARP_SIZE)
//                         {
//                             unsigned char hybidx = d_hybIdx[hybidxoffset + (rj >> 1)];
//                             int hybcol = rj % 2 == 0 ? (hybidx & num_f) >> 4 : hybidx & num_b;

//                             const MAT_VAL_TYPE r_x_gathered = __shfl_sync(0xffffffff, r_x, hybcol);
//                             sum += d_Blockhyb_Val[hybwoffset + rj] * r_x_gathered;
//                         }
//                         sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

//                         if (lane_id < BLOCK_SIZE)
//                             sumsum += sum;

//                         // // then do COO (the above code can be called)
//                         // if (lane_id < BLOCK_SIZE)
//                         // {
//                         //     // s_y_warp[lane_id] = 0;
//                         //     s_x_warp[lane_id] = r_x;
//                         // }

//                         // hybidxoffset += elllen >> 1; /// 2;
//                         // hybidxoffset += elllen % 2;

//                         // // int blknnz = s_blknnznnz_local[blkj - rowblkjstart];
//                         // // int nnzcoo = blknnz - elllen;

//                         // int nnzcoo = s_blknnznnz_local[blkj - rowblkjstart] - elllen;
//                         // for (int bnnzid = lane_id; bnnzid < nnzcoo; bnnzid += WARP_SIZE)
//                         // {
//                         //     unsigned char hybidx = d_hybIdx[hybidxoffset + bnnzid];
//                         //     unsigned char row = (hybidx & num_f) >> 4;
//                         //     unsigned char col = hybidx & num_b;

//                         //     MAT_VAL_TYPE r_x = d_Blockhyb_Val[elllen + hybwoffset + bnnzid] * s_x_warp[col];
//                         //     atomicAdd(&s_y_warp[row], r_x);
//                         // }

//                         // if (lane_id < BLOCK_SIZE)
//                         //     sumsum += s_y_warp[lane_id];
//                     }

//                     break;
                case 4:
                    // if dense (or near dense stored as dense)
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int denseoffset = d_ptroffset1[blkj];

                        MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? d_x[x_offset + lane_id] : 0;
                        // MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? s_x_warp[lane_id] : 0;
                        r_x = __shfl_up_sync(0xffffffff, r_x, BLOCK_SIZE);
                        int xoff1 = lane_id >> 4;

                        MAT_VAL_TYPE r_x_gathered;
                        int val_offset;
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1);
                        val_offset = denseoffset + lane_id;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 2);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 4);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 6);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 8);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 10);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 12);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 14);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];

                        sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

                        sumsum += sum;
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        // __syncwarp();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                case 5:
                    {
                        int dnsrowoffset = d_ptroffset1[blkj];
                        MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? d_x[x_offset + lane_id] : 0;
                        // MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? s_x_warp[lane_id] : 0;
                        r_x = __shfl_up_sync(0xffffffff, r_x, BLOCK_SIZE);

                        int subwarp_id = lane_id / BLOCK_SIZE;
                        int subwaprlane_id = (BLOCK_SIZE - 1) & lane_id;

                        int dnsrowptr = d_dnsrowptr[blkj];
                        for (int ri = dnsrowptr + subwarp_id; ri < d_dnsrowptr[blkj + 1]; ri += 2)
                        {
                            MAT_VAL_TYPE r_product = r_x *
                                                     d_Blockdenserow_Val[dnsrowoffset + (ri - dnsrowptr) * collength + subwaprlane_id];

                            if (lane_id < BLOCK_SIZE)
                            {
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 8);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 4);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 2);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 1);
                            }
                            else
                            {
                                r_product += __shfl_down_sync(0xffff0000, r_product, 8);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 4);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 2);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 1);
                            }

                            if (!subwaprlane_id)
                                atomicAdd(&d_left_sum[rowoffset + d_denserowid[ri]], r_product);
                        }
                        __threadfence();
                        // __syncwarp();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                case 6:
                    // if dense col (only work when the #nonzeros in the block is multiple of BLOCK_SIZE)
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int dnscoloffset = d_ptroffset1[blkj];
                        int colptrstart = d_dnscolptr[blkj];
                        int dnswidth = d_dnscolptr[blkj + 1] - colptrstart;

                        for (int glid = lane_id; glid < dnswidth * BLOCK_SIZE; glid += WARP_SIZE)
                        {
                            int rj = glid >> 4;
                            int ri = glid % BLOCK_SIZE;
                            ri += dnscoloffset;
                            ri += rj * BLOCK_SIZE;
                            rj += colptrstart;
                            rj = d_densecolid[rj];
                            rj += x_offset;
                            sum += d_Blockdensecol_Val[ri] * d_x[rj];
                        }
                        sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

                        sumsum += sum;
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        // __syncwarp();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                }

                // __threadfence();
                // __threadfence();
                // __syncwarp();
                // __syncwarp();
                // __syncwarp();
                // __syncwarp();
                // printf("tid = %d\n", global_id);
        }
        printf("turn 1 over\n");


// for (int i = 0; i < BLOCK_SIZE; i++)
//             __syncwarp();        // for (int i = 0; i < )
        // __threadfence();
        // __threadfence();
        // __threadfence();
    // // Producer
    // const int start_ptr = substitution == SUBSTITUTION_FORWARD ? 
    //                       d_cscColPtr[global_x_id]+1 : d_cscColPtr[global_x_id];
    // const int stop_ptr  = substitution == SUBSTITUTION_FORWARD ? 
    //                       d_cscColPtr[global_x_id+1] : d_cscColPtr[global_x_id+1]-1;
    // for (int jj = start_ptr + lane_id; jj < stop_ptr; jj += WARP_SIZE)
    // {
    //     const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
    //     const int rowIdx = d_cscRowIdx[j];

    //     atomicAdd(&d_left_sum[rowIdx], xi * d_cscVal[j]);
    //     __threadfence();
    //     atomicSub(&d_graphInDegree[rowIdx], 1);
    // }

    //finish
    // if (lane_id < BLOCK_SIZE) d_x[] = s_x_warp[lane_id];
}

__global__
void tilesptrsv_syncfree_cuda_executor_update_o(const int*         d_cscColPtr,
                                          const int*         d_cscRowIdx,
                                          const MAT_VAL_TYPE*  d_cscVal,
                                          int*                           d_graphInDegree,
                                          MAT_VAL_TYPE*                    d_left_sum,
                                          const int                      m,
                                          const int                      substitution,
                                          const MAT_VAL_TYPE*  d_b,
                                          MAT_VAL_TYPE*                    d_x,
                                          int*                           d_while_profiler,
                                          int*                           d_id_extractor,

                                          int*                           d_csctile_ptr,
                                          int*                           d_tile_rowidx,
                                          int*                           d_reflection,
                                          int*                           d_ptroffset1,
                                          int*                           d_ptroffset2,
                                          unsigned char*                           d_Blockcsr_Ptr,
                                          MAT_VAL_TYPE*                           d_Blockcsr_Val,
                                          unsigned char*                           d_blknnznnz,
                                          unsigned char*                           d_csr_compressedIdx,
                                          char*                          d_Format,
                                        
                                         unsigned char *d_coo_compressed_Idx,
                                         MAT_VAL_TYPE *d_Blockcoo_Val,
                                         MAT_VAL_TYPE *d_Blockdense_Val,
                                         int *d_dnsrowptr,
                                         MAT_VAL_TYPE *d_Blockdenserow_Val,
                                         char *d_denserowid,
                                         int *d_dnscolptr,
                                         MAT_VAL_TYPE *d_Blockdensecol_Val,
                                         char *d_densecolid)
{
    const int global_id = blockIdx.x * blockDim.x + threadIdx.x;

    // ----------------------- adjust -------------------------
    // int workgroup_id = global_id >> 5;
    // if (workgroup_id >= workgroup_num) return;
    // int global_x_id = d_workgroup_colidx[workgroup_id];
    // --------------------------------------------------------

    // Initialize
    const int lane_id = (WARP_SIZE - 1) & threadIdx.x;
    int global_x_id = 0;
    if (!lane_id)
        global_x_id = atomicAdd(d_id_extractor, 1);
    global_x_id = __shfl_sync(0xffffffff, global_x_id, 0);

    if (global_x_id >= m) return;

    // substitution is forward or backward
    global_x_id = substitution == SUBSTITUTION_FORWARD ? 
                  global_x_id : m - 1 - global_x_id;
    
    // int first_workgroup_thiscol = d_colidx_workgroup_ptr[global_x_id];

    // Consumer
    do {
        __threadfence_block();
    }
    while (d_graphInDegree[global_x_id] != 1);

    int colblkid = global_x_id;
    // int first_workgroup_thiscol = d_colidx_workgroup_ptr[global_x_id];
    
    int x_offset = colblkid * BLOCK_SIZE;
    __shared__ MAT_VAL_TYPE s_x[WARP_PER_BLOCK * BLOCK_SIZE];
    const int local_warp_id = threadIdx.x >> 5;
    MAT_VAL_TYPE *s_x_warp = &s_x[local_warp_id * BLOCK_SIZE];
    if (lane_id < BLOCK_SIZE)
        s_x_warp[lane_id] = d_x[x_offset + lane_id];

    if (!lane_id)
    {
        int blkid = d_csctile_ptr[colblkid];
        // printf("work id = %d\n", workgroup_id);
        // int blkid = d_workgroup_startblkidx[first_workgroup_thiscol];
        int rowblkid = d_tile_rowidx[blkid];
        int ref_blkid = d_reflection[blkid];
        int csroffset = d_ptroffset1[ref_blkid];
        int csrcount = d_ptroffset2[ref_blkid];
        int rowlength = BLOCK_SIZE;
        for (int ri = 0; ri < rowlength; ri++)
        {
            int start = d_Blockcsr_Ptr[csrcount + ri];
            int stop = ri == rowlength - 1 ? d_blknnznnz[ref_blkid] : d_Blockcsr_Ptr[ri + 1 + csrcount];
            MAT_VAL_TYPE sum = 0;
            for (int rj = start; rj < stop - 1; rj++)
            {
                int csrcol = csroffset + rj;
                unsigned char csridx = d_csr_compressedIdx[csrcol >> 1];
                csrcol = csrcol % 2;
                csrcol = csrcol == 0 ? (csridx & num_f) >> 4 : csridx & num_b;
                // sum += d_x[x_offset + csrcol] * d_Blockcsr_Val[csroffset + rj];
                sum += s_x_warp[csrcol] * d_Blockcsr_Val[csroffset + rj];
                
            }
            d_x[rowblkid * BLOCK_SIZE + ri] = (d_b[rowblkid * BLOCK_SIZE + ri] - d_left_sum[rowblkid * BLOCK_SIZE + ri] - sum) / d_Blockcsr_Val[csroffset + stop - 1];
            s_x_warp[ri] = d_x[rowblkid * BLOCK_SIZE + ri];
            // printf("%d  dx = %.1lf  b = %.1lf   left = %.1lf    sum = %.1lf\n", rowblkid * BLOCK_SIZE + ri, d_x[rowblkid * BLOCK_SIZE + ri], d_b[rowblkid * BLOCK_SIZE + ri], d_left_sum[rowblkid * BLOCK_SIZE + ri], sum);
        }
    }
    __syncwarp();
    // __threadfence_block();

// int start = first_workgroup_thiscol == workgroup_id ? d_workgroup_startblkidx[workgroup_id] + 1 :
//                                                       d_workgroup_startblkidx[workgroup_id];
// if (!lane_id)
//     printf("workid = %d     start = %d  end = %d\n", workgroup_id, start, d_workgroup_endblkidx[workgroup_id]);
for (int j = d_csctile_ptr[colblkid] + 1; j < d_csctile_ptr[colblkid+1]; j++)
        {
            int blkj = d_reflection[j];
            char subformat = d_Format[blkj];
            int colid = colblkid;
            int collength = BLOCK_SIZE;
            int rowoffset = d_tile_rowidx[j] * BLOCK_SIZE;
            int rowtileoffset = d_tile_rowidx[j];

                switch (subformat)
                {
                // if CSR
                case 0:
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int csroffset = d_ptroffset1[blkj];
                        int csrcount = d_ptroffset2[blkj];


                        int ri = lane_id >> 1;
                        int virtual_lane_id = lane_id & 0x1;

                        int stop = ri == BLOCK_SIZE - 1 ? d_blknnznnz[blkj] : d_Blockcsr_Ptr[ri + 1 + csrcount];

                        for (int rj = d_Blockcsr_Ptr[csrcount + ri] + virtual_lane_id; rj < stop; rj += 2)
                        {
                            int csrcol = csroffset + rj;
                            unsigned char csridx = d_csr_compressedIdx[csrcol >> 1];
                            csrcol = csrcol % 2;
                            csrcol = csrcol == 0 ? (csridx & num_f) >> 4 : csridx & num_b;
                            // sum += d_x[x_offset + csrcol] * d_Blockcsr_Val[csroffset + rj];
                            sum += s_x_warp[csrcol] * d_Blockcsr_Val[csroffset + rj];
                        }

                        sum += __shfl_down_sync(0xffffffff, sum, 1);
                        sumsum += __shfl_down_sync(0xffffffff, sum, lane_id);
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);

                    }

                    break;
                    
                case 1:
                    {
                        
                                        int coooffset = d_ptroffset1[blkj] + lane_id;

                                        int blknnz = d_blknnznnz[blkj];
                                        unsigned char aidx = lane_id < blknnz ? d_coo_compressed_Idx[coooffset] : 0;
                                        MAT_VAL_TYPE aval = lane_id < blknnz ? d_Blockcoo_Val[coooffset] : 0;

                                        if (lane_id < blknnz)
                                        {
                                            // atomicAdd(&d_left_sum[rowoffset + ((aidx & num_f) >> 4)], aval * d_x[x_offset + (aidx & num_b)]);
                                            atomicAdd(&d_left_sum[rowoffset + ((aidx & num_f) >> 4)], aval * s_x_warp[(aidx & num_b)]);
                                        }
                                            
                                        __threadfence();
                                        if (!lane_id)
                                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
//                 case 2:
//                     // if ELL
// #if DEBUG_FORMATCOST
//                     if (formatprofile == 2 || formatprofile == -1)
// #endif
//                     {
//                         sum = 0;
//                         int ellwoffset = d_ptroffset1[blkj - rowblkjstart];

//                         MAT_VAL_TYPE r_x = lane_id < collength ? d_x[x_offset + lane_id] : 0;

//                         int elllen = d_tilewidth[blkj] * BLOCK_SIZE;
//                         for (int rj = lane_id; rj < elllen; rj += WARP_SIZE)
//                         {
//                             int ellcol = ellwoffset + rj;
//                             unsigned int ellidx = d_ell_compressedIdx[ellcol >> 1];
//                             ellcol = ellcol % 2;
//                             ellcol = ellcol == 0 ? (ellidx & num_f) >> 4 : ellidx & num_b;
//                             MAT_VAL_TYPE r_x_gathered = lane_id < WARP_SIZE ? __shfl_sync(0x0000ffff, r_x, ellcol) : __shfl_sync(0xffff0000, r_x, ellcol);
//                             sum += d_Blockell_Val[ellwoffset + rj] * r_x_gathered;
//                         }
//                         sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

//                         sumsum += sum;
//                     }

//                     break;
//                 case 3:
//                     // if ELL

// #if DEBUG_FORMATCOST
//                     if (formatprofile == 3 || formatprofile == -1)
// #endif
//                     {
//                         // first do ELL (the above code can be called)
//                         sum = 0;

//                         int hybwoffset = s_ptroffset1_local[blkj - rowblkjstart];
//                         int hybidxoffset = s_ptroffset2_local[blkj - rowblkjstart];

//                         const MAT_VAL_TYPE r_x = lane_id < collength ? d_x[x_offset + lane_id] : 0;

//                         int elllen = d_tilewidth[blkj] * BLOCK_SIZE;
//                         for (int rj = lane_id; rj < elllen; rj += WARP_SIZE)
//                         {
//                             unsigned char hybidx = d_hybIdx[hybidxoffset + (rj >> 1)];
//                             int hybcol = rj % 2 == 0 ? (hybidx & num_f) >> 4 : hybidx & num_b;

//                             const MAT_VAL_TYPE r_x_gathered = __shfl_sync(0xffffffff, r_x, hybcol);
//                             sum += d_Blockhyb_Val[hybwoffset + rj] * r_x_gathered;
//                         }
//                         sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

//                         if (lane_id < BLOCK_SIZE)
//                             sumsum += sum;

//                         // // then do COO (the above code can be called)
//                         // if (lane_id < BLOCK_SIZE)
//                         // {
//                         //     // s_y_warp[lane_id] = 0;
//                         //     s_x_warp[lane_id] = r_x;
//                         // }

//                         // hybidxoffset += elllen >> 1; /// 2;
//                         // hybidxoffset += elllen % 2;

//                         // // int blknnz = s_blknnznnz_local[blkj - rowblkjstart];
//                         // // int nnzcoo = blknnz - elllen;

//                         // int nnzcoo = s_blknnznnz_local[blkj - rowblkjstart] - elllen;
//                         // for (int bnnzid = lane_id; bnnzid < nnzcoo; bnnzid += WARP_SIZE)
//                         // {
//                         //     unsigned char hybidx = d_hybIdx[hybidxoffset + bnnzid];
//                         //     unsigned char row = (hybidx & num_f) >> 4;
//                         //     unsigned char col = hybidx & num_b;

//                         //     MAT_VAL_TYPE r_x = d_Blockhyb_Val[elllen + hybwoffset + bnnzid] * s_x_warp[col];
//                         //     atomicAdd(&s_y_warp[row], r_x);
//                         // }

//                         // if (lane_id < BLOCK_SIZE)
//                         //     sumsum += s_y_warp[lane_id];
//                     }

//                     break;
                case 4:
                    // if dense (or near dense stored as dense)
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int denseoffset = d_ptroffset1[blkj];

                        // MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? d_x[x_offset + lane_id] : 0;
                        MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? s_x_warp[lane_id] : 0;
                        r_x = __shfl_up_sync(0xffffffff, r_x, BLOCK_SIZE);
                        int xoff1 = lane_id >> 4;

                        MAT_VAL_TYPE r_x_gathered;
                        int val_offset;
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1);
                        val_offset = denseoffset + lane_id;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 2);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 4);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 6);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 8);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 10);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 12);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];
                        r_x_gathered = __shfl_sync(0xffffffff, r_x, xoff1 * BLOCK_SIZE + xoff1 + 14);
                        val_offset += WARP_SIZE;
                        sum += r_x_gathered * d_Blockdense_Val[val_offset];

                        sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

                        sumsum += sum;
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                case 5:
                    {
                        int dnsrowoffset = d_ptroffset1[blkj];
                        // MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? d_x[x_offset + lane_id] : 0;
                        MAT_VAL_TYPE r_x = lane_id < BLOCK_SIZE ? s_x_warp[lane_id] : 0;
                        r_x = __shfl_up_sync(0xffffffff, r_x, BLOCK_SIZE);

                        int subwarp_id = lane_id / BLOCK_SIZE;
                        int subwaprlane_id = (BLOCK_SIZE - 1) & lane_id;

                        int dnsrowptr = d_dnsrowptr[blkj];
                        for (int ri = dnsrowptr + subwarp_id; ri < d_dnsrowptr[blkj + 1]; ri += 2)
                        {
                            MAT_VAL_TYPE r_product = r_x *
                                                     d_Blockdenserow_Val[dnsrowoffset + (ri - dnsrowptr) * collength + subwaprlane_id];

                            if (lane_id < BLOCK_SIZE)
                            {
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 8);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 4);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 2);
                                r_product += __shfl_down_sync(0x0000ffff, r_product, 1);
                            }
                            else
                            {
                                r_product += __shfl_down_sync(0xffff0000, r_product, 8);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 4);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 2);
                                r_product += __shfl_down_sync(0xffff0000, r_product, 1);
                            }

                            if (!subwaprlane_id)
                                atomicAdd(&d_left_sum[rowoffset + d_denserowid[ri]], r_product);
                        }
                        __threadfence();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                case 6:
                    // if dense col (only work when the #nonzeros in the block is multiple of BLOCK_SIZE)
                    {
                        MAT_VAL_TYPE sum = 0;
                        MAT_VAL_TYPE sumsum = 0;
                        int dnscoloffset = d_ptroffset1[blkj];
                        int colptrstart = d_dnscolptr[blkj];
                        int dnswidth = d_dnscolptr[blkj + 1] - colptrstart;

                        for (int glid = lane_id; glid < dnswidth * BLOCK_SIZE; glid += WARP_SIZE)
                        {
                            int rj = glid >> 4;
                            int ri = glid % BLOCK_SIZE;
                            ri += dnscoloffset;
                            ri += rj * BLOCK_SIZE;
                            rj += colptrstart;
                            rj = d_densecolid[rj];
                            // rj += x_offset;
                            sum += d_Blockdensecol_Val[ri] * s_x_warp[rj];
                        }
                        sum += __shfl_down_sync(0xffffffff, sum, BLOCK_SIZE);

                        sumsum += sum;
                        if (lane_id < BLOCK_SIZE)
                        {
                            atomicAdd(&d_left_sum[rowoffset+lane_id], sumsum);
                        }
                        __threadfence();
                        if (!lane_id)
                        atomicSub(&d_graphInDegree[rowtileoffset], 1);
                    }

                    break;
                }
        }
    // // Producer
    // const int start_ptr = substitution == SUBSTITUTION_FORWARD ? 
    //                       d_cscColPtr[global_x_id]+1 : d_cscColPtr[global_x_id];
    // const int stop_ptr  = substitution == SUBSTITUTION_FORWARD ? 
    //                       d_cscColPtr[global_x_id+1] : d_cscColPtr[global_x_id+1]-1;
    // for (int jj = start_ptr + lane_id; jj < stop_ptr; jj += WARP_SIZE)
    // {
    //     const int j = substitution == SUBSTITUTION_FORWARD ? jj : stop_ptr - 1 - (jj - start_ptr);
    //     const int rowIdx = d_cscRowIdx[j];

    //     atomicAdd(&d_left_sum[rowIdx], xi * d_cscVal[j]);
    //     __threadfence();
    //     atomicSub(&d_graphInDegree[rowIdx], 1);
    // }

    //finish
    // if (lane_id < BLOCK_SIZE) d_x[] = s_x_warp[lane_id];
}



int tilesptrsv_syncfree_cuda(const int       *cscColPtrTR,
                         const int           *cscRowIdxTR,
                         const MAT_VAL_TYPE    *cscValTR,
                         Tile_matrix *matrix,
                         const int            m,
                         const int            n,
                         const int            nnzTR,
                         const int            substitution,
                         const int            rhs,
                         const int            opt,
                               MAT_VAL_TYPE    *x,
                         const MAT_VAL_TYPE    *b,
                         const MAT_VAL_TYPE    *x_ref,
                         char                *filename,
                         int *ptroffset1,
                         int *ptroffset2,
                         double *time_conversion,
                         long long *tile_malloc_size)
{
    if (m != n)
    {
        printf("This is not a square matrix, return.\n");
        return -1;
    }

    int tilenum = matrix->tilenum;
    int tilem = matrix->tilem;
    int tilen = matrix->tilen;
    int *tile_ptr = matrix->tile_ptr;
    int *tile_columnidx = matrix->tile_columnidx;
    int *csctile_ptr = matrix->csctile_ptr;
    int *tile_rowidx = matrix->tile_rowidx;
    int *reflection = (int *)malloc(sizeof(int) * tilenum);
    // printf("m = %d  n = %d\n", m, n);
    // for (int i = 0; i < tilenum; i++)
    //     printf("%d\n", tile_rowidx[i]);
    int tilenum_percol_ave = tilenum / tilen;
    int tilenum_percol_best = 0;
    for (int i = 0; i < tilen; i++)
    {
        int length = csctile_ptr[i+1] - csctile_ptr[i];
        if (length > tilenum_percol_best)
            tilenum_percol_best = length;
        for (int j = csctile_ptr[i]; j < csctile_ptr[i+1]; j++)
        {
            int rowid = tile_rowidx[j];
            int colid = i;
            int count = 0;
            for (int k = tile_ptr[rowid]; k < tile_ptr[rowid+1]; k++)
                if (tile_columnidx[k] < colid)
                    count++;
            reflection[j] = tile_ptr[rowid] + count;
            // printf("row = %d  %d  %d  xxx = %d\n", rowid, tile_ptr[rowid], count, reflection[j]);
        }
    }
    // printf("tilenum ave = %d    tilenum best = %d\n", tilenum_percol_ave, tilenum_percol_best);

    int workgroup_num = 0;
    int *colidx_workgroup_ptr = (int *)malloc(sizeof(int) * (tilen + 1));
    colidx_workgroup_ptr[0] = 0;
    for (int blki = 0; blki < tilen; blki++)
    {
        int balancenumblk = csctile_ptr[blki + 1] - csctile_ptr[blki];
        if (balancenumblk <= PREFETCH_SMEM_TH)
            workgroup_num++;
        else
        {
            workgroup_num += ceil((double)balancenumblk / (double)PREFETCH_SMEM_TH);
        }
        colidx_workgroup_ptr[blki + 1] = workgroup_num;
    }
    int *workgroup_colidx = (int *)malloc(sizeof(int) * workgroup_num);
    int *workgroup_startblkidx = (int *)malloc(sizeof(int) * workgroup_num);
    int *workgroup_endblkidx = (int *)malloc(sizeof(int) * workgroup_num);

    int workgroup_id = 0;
    for (int blki = 0; blki < tilen; blki++)
    {
        int balancenumblk = csctile_ptr[blki + 1] - csctile_ptr[blki];
        if (balancenumblk <= PREFETCH_SMEM_TH)
        {
            workgroup_colidx[workgroup_id] = blki;
            workgroup_startblkidx[workgroup_id] = csctile_ptr[blki];
            workgroup_endblkidx[workgroup_id] = csctile_ptr[blki + 1];
                // printf("%d start = %d  end = %d\n", workgroup_id, workgroup_startblkidx[workgroup_id], workgroup_endblkidx[workgroup_id]);
            workgroup_id++;
        }
        else
        {
            for (int i = csctile_ptr[blki]; i < csctile_ptr[blki + 1]; i += PREFETCH_SMEM_TH)
            {
                workgroup_colidx[workgroup_id] = blki;
                workgroup_startblkidx[workgroup_id] = i;
                workgroup_endblkidx[workgroup_id] = (i + PREFETCH_SMEM_TH) >= csctile_ptr[blki + 1] ?
                                                    (csctile_ptr[blki + 1]) : (i + PREFETCH_SMEM_TH);
                // printf("%d start = %d  end = %d\n", workgroup_id, workgroup_startblkidx[workgroup_id], workgroup_endblkidx[workgroup_id]);
                workgroup_id++;
                // printf("w g repeat = %d\n", workgroup_id-1);
            }
        }
    }

    int *d_workgroup_colidx;
    int *d_workgroup_startblkidx;
    int *d_workgroup_endblkidx;
    int *d_colidx_workgroup_ptr;

    cudaMalloc((void **)&d_colidx_workgroup_ptr, sizeof(int) * (tilen + 1));
    cudaMemcpy(d_colidx_workgroup_ptr, colidx_workgroup_ptr, sizeof(int) * (tilen + 1), cudaMemcpyHostToDevice);

    cudaMalloc((void **)&d_workgroup_startblkidx, sizeof(int) * workgroup_num);
    cudaMemcpy(d_workgroup_startblkidx, workgroup_startblkidx, sizeof(int) * workgroup_num, cudaMemcpyHostToDevice);

    cudaMalloc((void **)&d_workgroup_endblkidx, sizeof(int) * workgroup_num);
    cudaMemcpy(d_workgroup_endblkidx, workgroup_endblkidx, sizeof(int) * workgroup_num, cudaMemcpyHostToDevice);

    cudaMalloc((void **)&d_workgroup_colidx, sizeof(int) * workgroup_num);
    cudaMemcpy(d_workgroup_colidx, workgroup_colidx, sizeof(int) * workgroup_num, cudaMemcpyHostToDevice);


    // transfer host mem to device mem
    int *d_cscColPtrTR;
    int *d_cscRowIdxTR;
    MAT_VAL_TYPE *d_cscValTR;
    MAT_VAL_TYPE *d_b;
    MAT_VAL_TYPE *d_x;

    // Matrix L
    // cudaMalloc((void **)&d_cscColPtrTR, (n+1) * sizeof(int));
    // cudaMalloc((void **)&d_cscRowIdxTR, nnzTR  * sizeof(int));
    // cudaMalloc((void **)&d_cscValTR,    nnzTR  * sizeof(MAT_VAL_TYPE));

    // cudaMemcpy(d_cscColPtrTR, cscColPtrTR, (n+1) * sizeof(int),   cudaMemcpyHostToDevice);
    // cudaMemcpy(d_cscRowIdxTR, cscRowIdxTR, nnzTR  * sizeof(int),   cudaMemcpyHostToDevice);
    // cudaMemcpy(d_cscValTR,    cscValTR,    nnzTR  * sizeof(MAT_VAL_TYPE),   cudaMemcpyHostToDevice);

    // Vector b
    cudaMalloc((void **)&d_b, m * rhs * sizeof(MAT_VAL_TYPE));
    cudaMemcpy(d_b, b, m * rhs * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);

    // Vector x
    cudaMalloc((void **)&d_x, n * rhs * sizeof(MAT_VAL_TYPE));
    cudaMemset(d_x, 0, n * rhs * sizeof(MAT_VAL_TYPE));

    //  - cuda syncfree SpTRSV analysis start!
    printf(" - cuda syncfree SpTRSV analysis start!\n");

    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    // malloc tmp memory to generate in-degree
    int *d_graphInDegree;
    int *d_graphInDegree_backup;
    cudaMalloc((void **)&d_graphInDegree, tilem * sizeof(int));
    cudaMalloc((void **)&d_graphInDegree_backup, tilem * sizeof(int));

    int *d_id_extractor;
    cudaMalloc((void **)&d_id_extractor, sizeof(int));

    int *d_tile_rowidx;
    cudaMalloc((void **)&d_tile_rowidx,    tilenum  * sizeof(int));
    cudaMemcpy(d_tile_rowidx, tile_rowidx, (tilenum) * sizeof(int),   cudaMemcpyHostToDevice);
    int num_threads = 128;
    int num_blocks = ceil ((double)tilenum / (double)num_threads);
    for (int i = 0; i < 1; i++)
    {
        cudaMemset(d_graphInDegree, 0, tilem * sizeof(int));
        tilesptrsv_syncfree_cuda_analyser<<< num_blocks, num_threads >>>
                                      (d_tile_rowidx, tilem, tilenum, d_graphInDegree);
    }
    cudaDeviceSynchronize();

    gettimeofday(&t2, NULL);
    double time_cuda_analysis = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    time_cuda_analysis /= 1;
    // *preprocessing_syncfree = time_cuda_analysis;

    printf("cuda syncfree SpTRSV analysis on L used %4.2f ms\n", time_cuda_analysis);

    //  - cuda syncfree SpTRSV solve start!
    printf(" - cuda syncfree SpTRSV solve start!\n");

    // malloc tmp memory to collect a partial sum of each row
    MAT_VAL_TYPE *d_left_sum;
    cudaMalloc((void **)&d_left_sum, sizeof(MAT_VAL_TYPE) * m * rhs);

    // backup in-degree array, only used for benchmarking multiple runs
    cudaMemcpy(d_graphInDegree_backup, d_graphInDegree, tilem * sizeof(int), cudaMemcpyDeviceToDevice);

    // this is for profiling while loop only
    int *d_while_profiler;
    cudaMalloc((void **)&d_while_profiler, sizeof(int) * tilen);
    cudaMemset(d_while_profiler, 0, sizeof(int) * tilen);
    int *while_profiler = (int *)malloc(sizeof(int) * tilen);


    int csrsize = matrix->csrsize;
    int csrptrlen = matrix->csrptrlen;
    int csr_csize = csrsize % 2 == 0 ? csrsize / 2 : csrsize / 2 + 1;

    // int *csctile_ptr = matrix->csctile_ptr;
    // int *tile_rowidx = matrix->tile_rowidx;
    // int *ptroffset1 = matrix->ptroffset1;
    // int *ptroffset2 = matrix->ptroffset2;
    unsigned char *Blockcsr_Ptr = matrix->Blockcsr_Ptr;
    MAT_VAL_TYPE *Blockcsr_Val = matrix->Blockcsr_Val;
    unsigned char *blknnznnz = matrix->blknnznnz;
    unsigned char *csr_compressedIdx = matrix->csr_compressedIdx;
    char *Format = matrix->Format;

    int *d_reflection;
    int *d_csctile_ptr;
    // int *d_tile_rowidx;
    int *d_ptroffset1;
    int *d_ptroffset2;
    unsigned char *d_Blockcsr_Ptr;
    MAT_VAL_TYPE *d_Blockcsr_Val;
    unsigned char *d_blknnznnz;
    unsigned char *d_csr_compressedIdx;
    char *d_Format;
    cudaMalloc((void **)&d_reflection, tilenum * sizeof(int));
    cudaMalloc((void **)&d_csctile_ptr, (tilen + 1) * sizeof(MAT_PTR_TYPE));
    // cudaMalloc((void **)&d_tile_rowidx, tilenum * sizeof(int));
    cudaMalloc((void **)&d_ptroffset1, tilenum * sizeof(int));
    cudaMalloc((void **)&d_ptroffset2, tilenum * sizeof(int));
    cudaMalloc((void **)&d_csr_compressedIdx, (csr_csize) * sizeof(unsigned char));
    cudaMalloc((void **)&d_Blockcsr_Val, (csrsize) * sizeof(MAT_VAL_TYPE));
    cudaMalloc((void **)&d_Blockcsr_Ptr, (csrptrlen) * sizeof(unsigned char));
    cudaMalloc((void **)&d_blknnznnz, (tilenum + 1) * sizeof(unsigned char));
    cudaMalloc((void **)&d_Format, tilenum * sizeof(char));

    cudaMemcpy(d_reflection, reflection, tilenum * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_csr_compressedIdx, csr_compressedIdx, (csr_csize) * sizeof(unsigned char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockcsr_Val, Blockcsr_Val, (csrsize) * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockcsr_Ptr, Blockcsr_Ptr, (csrptrlen) * sizeof(unsigned char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Format, Format, tilenum * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_blknnznnz, blknnznnz, (tilenum + 1) * sizeof(unsigned char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_csctile_ptr, csctile_ptr, (tilen + 1) * sizeof(MAT_PTR_TYPE), cudaMemcpyHostToDevice);
    // cudaMemcpy(d_tile_rowidx, tile_rowidx, tilenum * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ptroffset1, ptroffset1, tilenum * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ptroffset2, ptroffset2, tilenum * sizeof(int), cudaMemcpyHostToDevice);

    unsigned char *coo_compressed_Idx = matrix->coo_compressed_Idx;
    MAT_VAL_TYPE *Blockcoo_Val = matrix->Blockcoo_Val;
    MAT_VAL_TYPE *Blockdense_Val = matrix->Blockdense_Val;
    int *dnsrowptr = matrix->dnsrowptr;
    MAT_VAL_TYPE *Blockdenserow_Val = matrix->Blockdenserow_Val;
    char *denserowid = matrix->denserowid;
    MAT_VAL_TYPE *Blockdensecol_Val = matrix->Blockdensecol_Val;
    char *densecolid = matrix->densecolid;
    int *dnscolptr = matrix->dnscolptr;
    int coosize = matrix->coosize;
    int dense_size = matrix->dnssize;
    int denserow_size = matrix->dnsrowsize;
    int densecol_size = matrix->dnscolsize;

    unsigned char *d_coo_compressed_Idx;
    MAT_VAL_TYPE *d_Blockcoo_Val;
    MAT_VAL_TYPE *d_Blockdense_Val;
    int *d_dnsrowptr;
    MAT_VAL_TYPE *d_Blockdenserow_Val;
    char *d_denserowid;
    int *d_dnscolptr;
    MAT_VAL_TYPE *d_Blockdensecol_Val;
    char *d_densecolid;

    cudaMalloc((void **)&d_coo_compressed_Idx, (coosize) * sizeof(unsigned char));
    cudaMalloc((void **)&d_Blockcoo_Val, (coosize) * sizeof(MAT_VAL_TYPE));
    cudaMalloc((void **)&d_Blockdense_Val, (dense_size) * sizeof(MAT_VAL_TYPE));
    cudaMalloc((void **)&d_dnsrowptr, (tilenum + 1) * sizeof(int));
    cudaMalloc((void **)&d_Blockdenserow_Val, (denserow_size) * sizeof(MAT_VAL_TYPE));
    cudaMalloc((void **)&d_denserowid, dnsrowptr[tilenum] * sizeof(char));
    cudaMalloc((void **)&d_dnscolptr, (tilenum + 1) * sizeof(int));
    cudaMalloc((void **)&d_Blockdensecol_Val, (densecol_size) * sizeof(MAT_VAL_TYPE));
    cudaMalloc((void **)&d_densecolid, dnscolptr[tilenum] * sizeof(char));

    cudaMemcpy(d_coo_compressed_Idx, coo_compressed_Idx, (coosize) * sizeof(unsigned char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockcoo_Val, Blockcoo_Val, (coosize) * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockdense_Val, Blockdense_Val, (dense_size) * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dnsrowptr, dnsrowptr, (tilenum + 1) * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockdenserow_Val, Blockdenserow_Val, (denserow_size) * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);
    cudaMemcpy(d_denserowid, denserowid, dnsrowptr[tilenum] * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dnscolptr, dnscolptr, (tilenum + 1) * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Blockdensecol_Val, Blockdensecol_Val, (densecol_size) * sizeof(MAT_VAL_TYPE), cudaMemcpyHostToDevice);
    cudaMemcpy(d_densecolid, densecolid, dnscolptr[tilenum] * sizeof(char), cudaMemcpyHostToDevice);

    int *d_graphInDegree_diag;
    cudaMalloc((void **)&d_graphInDegree_diag, sizeof(int) * (tilem) * BLOCK_SIZE);
    // for (int i = 0; i < WARMUP_NUM; i++)
    // {
    //     cudaMemcpy(d_graphInDegree_diag, matrix->graphInDegree, sizeof(int) * (tilem) * BLOCK_SIZE, cudaMemcpyHostToDevice);
    //     // get a unmodified in-degree array, only for benchmarking use
    //     cudaMemcpy(d_graphInDegree, d_graphInDegree_backup, tilem * sizeof(int), cudaMemcpyDeviceToDevice);
    //     //cudaMemset(d_graphInDegree, 0, sizeof(int) * m);
        
    //     // clear left_sum array, only for benchmarking use
    //     cudaMemset(d_left_sum, 0, sizeof(MAT_VAL_TYPE) * m * rhs);
    //     cudaMemset(d_x, 0, sizeof(MAT_VAL_TYPE) * n * rhs);
    //     cudaMemset(d_id_extractor, 0, sizeof(int));

    //     // if (rhs == 1)
    //     // {
    //         num_threads = WARP_PER_BLOCK * WARP_SIZE;
    //         //num_threads = 1 * WARP_SIZE;
    //         num_blocks = ceil ((double)workgroup_num / (double)(num_threads/WARP_SIZE));
    //         // printf(".....\n");
    //         //sptrsv_syncfree_cuda_executor<<< num_blocks, num_threads >>>
    //         tilesptrsv_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
    //                                      (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
    //                                       d_graphInDegree, d_left_sum,
    //                                       tilem, substitution, d_b, d_x, d_while_profiler, d_id_extractor,
    //                                       d_csctile_ptr, d_tile_rowidx, d_reflection, d_ptroffset1, 
    //                                       d_ptroffset2, d_Blockcsr_Ptr, d_Blockcsr_Val, d_blknnznnz, 
    //                                       d_csr_compressedIdx, d_Format, d_coo_compressed_Idx, d_Blockcoo_Val,
    //                                       d_Blockdense_Val, d_dnsrowptr, d_Blockdenserow_Val, d_denserowid, 
    //                                       d_dnscolptr, d_Blockdensecol_Val, d_densecolid, d_workgroup_colidx,
    //                                       d_colidx_workgroup_ptr, d_workgroup_startblkidx, d_workgroup_endblkidx, workgroup_num, d_graphInDegree_diag);
    //         // printf("yyyyyy\n");
    //     // }
    //     // else
    //     // {
    //     //     num_threads = 4 * WARP_SIZE;
    //     //     num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
    //     //     sptrsm_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
    //     //                                  (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
    //     //                                   d_graphInDegree, d_left_sum,
    //     //                                   m, substitution, rhs, opt,
    //     //                                   d_b, d_x, d_while_profiler, d_id_extractor);
    //     // }
    //     cudaDeviceSynchronize();
    // }    

    // step 5: solve L*y = x
    double time_cuda_solve = 0;
    // printf("ttttt\n");

    for (int i = 0; i < BENCH_REPEAT; i++)
    {
        cudaMemcpy(d_graphInDegree_diag, matrix->graphInDegree, sizeof(int) * (tilem) * BLOCK_SIZE, cudaMemcpyHostToDevice);
        // get a unmodified in-degree array, only for benchmarking use
        cudaMemcpy(d_graphInDegree, d_graphInDegree_backup, tilem * sizeof(int), cudaMemcpyDeviceToDevice);
        // cudaMemset(d_graphInDegree, 0, sizeof(int) * m);
        
        // clear left_sum array, only for benchmarking use
        cudaMemset(d_left_sum, 0, sizeof(MAT_VAL_TYPE) * m * rhs);
        cudaMemset(d_x, 0, sizeof(MAT_VAL_TYPE) * n * rhs);
        cudaMemset(d_id_extractor, 0, sizeof(int));

        gettimeofday(&t1, NULL);

        // if (rhs == 1)
        // {
            num_threads = WARP_PER_BLOCK * WARP_SIZE;
            //num_threads = 1 * WARP_SIZE;
            num_blocks = ceil ((double)workgroup_num / (double)(num_threads/WARP_SIZE));
            //sptrsv_syncfree_cuda_executor<<< num_blocks, num_threads >>>
            
            tilesptrsv_syncfree_cuda_executor_update_o<<< num_blocks, num_threads >>>
                                         (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
                                          d_graphInDegree, d_left_sum,
                                          tilem, substitution, d_b, d_x, d_while_profiler, d_id_extractor,
                                          d_csctile_ptr, d_tile_rowidx, d_reflection, d_ptroffset1, 
                                          d_ptroffset2, d_Blockcsr_Ptr, d_Blockcsr_Val, d_blknnznnz, 
                                          d_csr_compressedIdx, d_Format, d_coo_compressed_Idx, d_Blockcoo_Val,
                                          d_Blockdense_Val, d_dnsrowptr, d_Blockdenserow_Val, d_denserowid, 
                                          d_dnscolptr, d_Blockdensecol_Val, d_densecolid);
        // }
        // else
        // {
        //     num_threads = 4 * WARP_SIZE;
        //     num_blocks = ceil ((double)m / (double)(num_threads/WARP_SIZE));
        //     sptrsm_syncfree_cuda_executor_update<<< num_blocks, num_threads >>>
        //                                  (d_cscColPtrTR, d_cscRowIdxTR, d_cscValTR,
        //                                   d_graphInDegree, d_left_sum,
        //                                   m, substitution, rhs, opt,
        //                                   d_b, d_x, d_while_profiler, d_id_extractor);
        // }

        cudaDeviceSynchronize();
        gettimeofday(&t2, NULL);

        time_cuda_solve += (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0;
    }

    time_cuda_solve /= BENCH_REPEAT;
    double flop = 2*(double)rhs*(double)nnzTR;

    printf("cuda syncfree SpTRSV solve used %4.2f ms, throughput is %4.2f gflops\n",
           time_cuda_solve, flop/(1e6*time_cuda_solve));
    double gflops = flop/(1e6*time_cuda_solve);

    // int prar_fake = 0;
    // FILE *fout = fopen("res/4090/tilesptrsv_syncfree.csv", "a");
    // fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f",
    //         m, n, nnzTR, prar_fake, *tile_malloc_size, *time_conversion, time_cuda_solve, gflops);
    // fclose(fout);

    // FILE *fout = fopen("syncfreebal_tile.csv", "a");
    // if (fout == NULL)
    //     printf("Writing results fails.\n");
    // fprintf(fout, "%s,%i,%i,%i,%f,%f\n",
    //         filename, m, n, nnzTR, time_cuda_solve, gflops);
    // fclose(fout);

    cudaMemcpy(x, d_x, n * rhs * sizeof(MAT_VAL_TYPE), cudaMemcpyDeviceToHost);

    // validate x
    double accuracy = 1e-4;
    double ref = 0.0;
    double res = 0.0;

    for (int i = 0; i < n * rhs; i++)
    {
        ref += abs(x_ref[i]);
        res += abs(x[i] - x_ref[i]);
        //if (x_ref[i] != x[i]) printf ("[%i, %i] x_ref = %f, x = %f\n", i/rhs, i%rhs, x_ref[i], x[i]);
    }
    res = ref == 0 ? res : res / ref;

    if (res < accuracy)
        printf("cuda tilesptrsv syncfree SpTRSV executor passed! |x-xref|/|xref| = %8.2e\n", res);
    else
        printf("cuda tilesptrsv syncfree SpTRSV executor _NOT_ passed! |x-xref|/|xref| = %8.2e\n", res);

    int prar_fake = 0;
    FILE *fout = fopen("res/4090/tilesptrsv-syncfree.csv", "a");
    fprintf(fout, ",%i,%i,%i,%i, %lli, %f, %f,%f, %f",
            m, n, nnzTR, prar_fake, *tile_malloc_size, *time_conversion, time_cuda_solve, gflops, res);
    fclose(fout);

    // profile while loop
    cudaMemcpy(while_profiler, d_while_profiler, n * sizeof(int), cudaMemcpyDeviceToHost);
    long long unsigned int while_count = 0;
    for (int i = 0; i < n; i++)
    {
        while_count += while_profiler[i];
        //printf("while_profiler[%i] = %i\n", i, while_profiler[i]);
    }
    //printf("\nwhile_count= %llu in total, %llu per row/column\n", while_count, while_count/m);

    // step 6: free resources
    free(while_profiler);

    cudaFree(d_graphInDegree);
    cudaFree(d_graphInDegree_backup);
    cudaFree(d_id_extractor);
    cudaFree(d_left_sum);
    cudaFree(d_while_profiler);

    cudaFree(d_cscColPtrTR);
    cudaFree(d_cscRowIdxTR);
    cudaFree(d_cscValTR);
    cudaFree(d_b);
    cudaFree(d_x);

    return 0;
}

#endif



